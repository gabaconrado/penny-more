.PHONY: test fmt clippy lint ci

fmt:
	cargo fmt

clippy:
	cargo clippy --all-features -- -D warnings
	cargo clippy --tests -- -D warnings

test:
	cargo test --all-features

lint: fmt clippy

check: lint test
