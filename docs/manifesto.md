# Penny-more Manifesto

## Simplicity above everything else

Managing money should be simple, nothing more than adding expenses, incomes and
transfers :).

## Control your currencies

Accounts in different currencies should be aggregated differently, no automatic
conversions is required.

## Control your data

Importing and exporting data should be simple and quick.
