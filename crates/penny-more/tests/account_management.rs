//! Integrated tests for account management related features

#[cfg(feature = "test-helpers")]
mod tests {

    use data_storage::InMemoryDatabase;
    use penny_more::{test_helpers, Account, AccountChangeSet, MonetaryValue};

    /// Happy-path simple test for account management. All functions must be
    /// successful and the side-effects should be verified.
    ///
    /// Starts with a clean system state:
    ///
    /// Steps:
    ///
    /// - Initialize objects;
    /// - Add an account;
    /// - Add a second account;
    /// - List and verify both accounts;
    /// - Updates the name and the balance of the second account;
    /// - Deletes the first account;
    /// - List and verify updated second account;
    #[test]
    fn account_common_flow() -> anyhow::Result<()> {
        // Init
        let data_source = InMemoryDatabase::default();
        let accounts = [Account::sample(), Account::sample()];

        // Add accounts
        penny_more::add_account_controller(&data_source, &accounts[0])?;
        penny_more::add_account_controller(&data_source, &accounts[1])?;

        // List and check
        let saved_accounts = penny_more::list_accounts_controller(&data_source)?;
        assert!(saved_accounts.iter().all(|sa| accounts.contains(sa)));

        // Update
        let new_name = test_helpers::random_string(16);
        let new_balance = MonetaryValue::sample();
        let account_cs =
            AccountChangeSet::new(accounts[1].id(), Some(new_name.clone()), Some(new_balance));
        penny_more::update_account_controller(&data_source, &data_source, account_cs)?;

        // Delete
        penny_more::delete_account_controller(&data_source, accounts[0].id())?;

        // List and check
        let expected_account = Account::new(accounts[1].id(), new_name, new_balance);
        let saved_accounts = penny_more::list_accounts_controller(&data_source)?;
        match saved_accounts.as_slice() {
            [acc] => assert_eq!(*acc, expected_account),
            _ => panic!("Saved accounts is different than expected"),
        };
        Ok(())
    }
}
