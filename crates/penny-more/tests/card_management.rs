//! Integrated tests for card management related features

#[cfg(feature = "test-helpers")]
mod tests {

    use data_storage::InMemoryDatabase;
    use penny_more::{test_helpers, Account, Card, CardChangeSet, MonetaryValue};

    /// Happy-path simple test for card management. All functions must be
    /// successful and the side-effects should be verified.
    ///
    /// Starts with a clean system state:
    ///
    /// Steps:
    ///
    /// - Initialize objects;
    /// - Add a card without an account;
    /// - Add a second card with account;
    /// - List and verify both cards;
    /// - Updates the account of the first card;
    /// - Updates the name and the limit of the second card;
    /// - List and verify updated cards;
    /// - Deletes the first card;
    /// - List and verify remaining card;
    #[test]
    fn card_common_flow() -> anyhow::Result<()> {
        // Init
        let data_source = InMemoryDatabase::default();
        let account = Account::sample();
        let mut cards = [Card::sample(), Card::sample()];
        cards[1].set_account(account.id());

        // Add cards
        penny_more::add_account_controller(&data_source, &account)?;
        penny_more::add_card_controller(&data_source, &data_source, &cards[0])?;
        penny_more::add_card_controller(&data_source, &data_source, &cards[1])?;

        // List and check
        let saved_cards = penny_more::list_cards_controller(&data_source)?;
        assert!(saved_cards.iter().all(|sc| cards.contains(sc)));

        // Update second
        let new_name = test_helpers::random_string(16);
        let new_limit = MonetaryValue::sample();
        let card_cs = CardChangeSet::new(
            cards[1].id(),
            Some(new_name.clone()),
            Some(new_limit),
            Some(None),
        );
        penny_more::update_card_controller(&data_source, &data_source, &data_source, card_cs)?;

        // List and check
        let expected_card = Card::new(cards[1].id(), new_name, new_limit, None);
        let saved_cards = penny_more::list_cards_controller(&data_source)?;
        assert!(saved_cards.contains(&cards[0]));
        assert!(saved_cards.contains(&expected_card));

        // Delete
        penny_more::delete_card_controller(&data_source, cards[1].id())?;

        // List and check
        let saved_cards = penny_more::list_cards_controller(&data_source)?;
        match saved_cards.as_slice() {
            [card] => assert_eq!(*card, cards[0]),
            _ => panic!("Saved accounts is different than expected"),
        };
        Ok(())
    }
}
