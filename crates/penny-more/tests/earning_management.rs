//! Integrated tests for earning management related features

#[cfg(feature = "test-helpers")]
mod tests {

    use data_storage::InMemoryDatabase;
    use penny_more::{Earning, EarningChangeSet, MonetaryValue};

    /// Happy-path simple test for earnings management. All functions must be
    /// successful and the side-effects should be verified.
    ///
    /// Starts with a clean system state:
    ///
    /// Steps:
    ///
    /// - Initialize objects;
    /// - Add two earnings;
    /// - List earnings and confirms it is added;
    /// - Update values in earning;
    /// - List and verify updated earnings;
    /// - Deletes an earning;
    /// - List and verify remaining card;
    #[test]
    fn earnings_common_flow() -> anyhow::Result<()> {
        // Init
        let data_source = InMemoryDatabase::default();
        let earnings = [Earning::sample(), Earning::sample()];

        // Add earnings
        penny_more::add_earning_controller(&data_source, &earnings[0])?;
        penny_more::add_earning_controller(&data_source, &earnings[1])?;

        // List and check
        let saved_earnings = penny_more::list_earnings_controller(&data_source)?;
        assert!(saved_earnings.iter().all(|se| earnings.contains(se)));

        // Update second
        let new_value = MonetaryValue::sample();
        let earning_cs = EarningChangeSet::new(earnings[1].id(), None, None, Some(new_value), None);
        penny_more::update_earning_controller(&data_source, &data_source, earning_cs)?;

        // List and check
        let expected_earning = Earning::new(
            earnings[1].id(),
            earnings[1].timestamp(),
            earnings[1].owner(),
            new_value,
            earnings[1].description().cloned(),
        );
        let saved_earnings = penny_more::list_earnings_controller(&data_source)?;
        assert!(saved_earnings.contains(&earnings[0]));
        assert!(saved_earnings.contains(&expected_earning));

        // Delete
        penny_more::delete_earning_controller(&data_source, earnings[1].id())?;

        // List and check
        let saved_earnings = penny_more::list_earnings_controller(&data_source)?;
        match saved_earnings.as_slice() {
            [earning] => assert_eq!(*earning, earnings[0]),
            _ => panic!("Saved earnings is different than expected"),
        };
        Ok(())
    }
}
