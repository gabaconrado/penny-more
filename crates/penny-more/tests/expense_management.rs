//! Integrated tests for expense management related features

#[cfg(feature = "test-helpers")]
mod tests {

    use data_storage::InMemoryDatabase;
    use penny_more::{Expense, ExpenseChangeSet, MonetaryValue};

    /// Happy-path simple test for expenses management. All functions must be
    /// successful and the side-effects should be verified.
    ///
    /// Starts with a clean system state:
    ///
    /// Steps:
    ///
    /// - Initialize objects;
    /// - Add two expenses;
    /// - List expenses and confirms it is added;
    /// - Update values in expense;
    /// - List and verify updated expenses;
    /// - Deletes an expense;
    /// - List and verify remaining card;
    #[test]
    fn expenses_common_flow() -> anyhow::Result<()> {
        // Init
        let data_source = InMemoryDatabase::default();
        let expenses = [Expense::sample(), Expense::sample()];

        // Add expenses
        penny_more::add_expense_controller(&data_source, &expenses[0])?;
        penny_more::add_expense_controller(&data_source, &expenses[1])?;

        // List and check
        let saved_expenses = penny_more::list_expenses_controller(&data_source)?;
        assert!(saved_expenses.iter().all(|se| expenses.contains(se)));

        // Update second
        let new_value = MonetaryValue::sample();
        let expense_cs = ExpenseChangeSet::new(expenses[1].id(), None, None, Some(new_value), None);
        penny_more::update_expense_controller(&data_source, &data_source, expense_cs)?;

        // List and check
        let expected_expense = Expense::new(
            expenses[1].id(),
            expenses[1].timestamp(),
            expenses[1].owner(),
            new_value,
            expenses[1].description().cloned(),
        );
        let saved_expenses = penny_more::list_expenses_controller(&data_source)?;
        assert!(saved_expenses.contains(&expenses[0]));
        assert!(saved_expenses.contains(&expected_expense));

        // Delete
        penny_more::delete_expense_controller(&data_source, expenses[1].id())?;

        // List and check
        let saved_expenses = penny_more::list_expenses_controller(&data_source)?;
        match saved_expenses.as_slice() {
            [expense] => assert_eq!(*expense, expenses[0]),
            _ => panic!("Saved expenses is different than expected"),
        };
        Ok(())
    }
}
