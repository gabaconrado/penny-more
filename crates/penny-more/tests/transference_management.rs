//! Integrated tests for transference management related features

#[cfg(feature = "test-helpers")]
mod tests {

    use data_storage::InMemoryDatabase;
    use penny_more::{MonetaryValue, Transference, TransferenceChangeSet};

    /// Happy-path simple test for transferences management. All functions must be
    /// successful and the side-effects should be verified.
    ///
    /// Starts with a clean system state:
    ///
    /// Steps:
    ///
    /// - Initialize objects;
    /// - Add two transferences;
    /// - List transferences and confirms it is added;
    /// - Update values in transference;
    /// - List and verify updated transferences;
    /// - Deletes an transference;
    /// - List and verify remaining card;
    #[test]
    fn transferences_common_flow() -> anyhow::Result<()> {
        // Init
        let data_source = InMemoryDatabase::default();
        let transferences = [Transference::sample(), Transference::sample()];

        // Add transferences
        penny_more::add_transference_controller(&data_source, &transferences[0])?;
        penny_more::add_transference_controller(&data_source, &transferences[1])?;

        // List and check
        let saved_transferences = penny_more::list_transferences_controller(&data_source)?;
        assert!(saved_transferences
            .iter()
            .all(|se| transferences.contains(se)));

        // Update second
        let new_value = MonetaryValue::sample();
        let transference_cs = TransferenceChangeSet::new(
            transferences[1].id(),
            None,
            None,
            None,
            Some(new_value),
            None,
        );
        penny_more::update_transference_controller(&data_source, &data_source, transference_cs)?;

        // List and check
        let expected_transference = Transference::new(
            transferences[1].id(),
            transferences[1].timestamp(),
            transferences[1].source(),
            transferences[1].target(),
            new_value,
            transferences[1].description().cloned(),
        );
        let saved_transferences = penny_more::list_transferences_controller(&data_source)?;
        assert!(saved_transferences.contains(&transferences[0]));
        assert!(saved_transferences.contains(&expected_transference));

        // Delete
        penny_more::delete_transference_controller(&data_source, transferences[1].id())?;

        // List and check
        let saved_transferences = penny_more::list_transferences_controller(&data_source)?;
        match saved_transferences.as_slice() {
            [transference] => assert_eq!(*transference, transferences[0]),
            _ => panic!("Saved transferences is different than expected"),
        };
        Ok(())
    }
}
