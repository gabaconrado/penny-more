# Penny-more core

Core crate of the Penny-more system, provides all business-logic related types,
traits, use cases and controllers;
