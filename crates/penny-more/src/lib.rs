#![doc = include_str!("../README.md")]
#![deny(
    missing_debug_implementations,
    missing_docs,
    trivial_casts,
    trivial_numeric_casts,
    unused_extern_crates,
    unused_results,
    clippy::unwrap_used,
    clippy::as_conversions,
    clippy::indexing_slicing
)]
// Allow mock objects to have a more flexible initialization by generating default based objects
// even if the type have only one field
#![cfg_attr(test, allow(clippy::needless_update))]

/************************ Main business-rules modules ************************/

/// All account-related functionality
mod accounts;
/// All card-related functionality
mod cards;
/// All transaction-related functionality
mod transactions;

/****************************** Support modules ******************************/

/// Useful structures that are shared across internal modules
mod generic;

/// Testing utilities
#[cfg(any(test, feature = "test-helpers"))]
pub mod test_helpers;

/****************************** Public objects *******************************/

// Identifiers proxy
mod ids {
    use crate::generic::types::identifier::Identifier;
    /// Identifier of an account
    pub type AccountId = Identifier<super::Account>;
    /// Identifier of a card
    pub type CardId = Identifier<super::Card>;
    /// Identifier of a transaction
    pub type TransactionId = Identifier<super::Transaction>;
}

pub use self::{
    // Account-related re-exports
    accounts::{
        add_account::{add_account_controller, AddAccount},
        delete_account::{delete_account_controller, DeleteAccount},
        read_account::{list_accounts_controller, ReadAccount},
        types::Account,
        update_account::{update_account_controller, AccountChangeSet, UpdateAccount},
    },
    // Cards-related re-exports
    cards::{
        add_card::{add_card_controller, AddCard},
        delete_card::{delete_card_controller, DeleteCard},
        read_card::{list_cards_controller, ReadCard},
        types::Card,
        update_card::{update_card_controller, CardChangeSet, UpdateCard},
    },
    // General re-exports
    generic::types::monetary_value::MonetaryValue,
    // Identifiers
    ids::{AccountId, CardId, TransactionId},
    // Transaction-related re-exports
    transactions::{
        earning_add::{add_earning_controller, AddEarning},
        earning_delete::{delete_earning_controller, DeleteEarning},
        earning_read::{list_earnings_controller, ReadEarning},
        earning_update::{update_earning_controller, EarningChangeSet, UpdateEarning},
        expense_add::{add_expense_controller, AddExpense},
        expense_delete::{delete_expense_controller, DeleteExpense},
        expense_read::{list_expenses_controller, ReadExpense},
        expense_update::{update_expense_controller, ExpenseChangeSet, UpdateExpense},
        transaction_add::{add_transaction_controller, AddTransaction},
        transaction_delete::{delete_transaction_controller, DeleteTransaction},
        transaction_read::{list_transactions_controller, ReadTransaction},
        transaction_update::{
            update_transaction_controller, TransactionChangeSet, UpdateTransaction,
        },
        transference_add::{add_transference_controller, AddTransference},
        transference_delete::{delete_transference_controller, DeleteTransference},
        transference_read::{list_transferences_controller, ReadTransference},
        transference_update::{
            update_transference_controller, TransferenceChangeSet, UpdateTransference,
        },
        types::{
            earning::Earning,
            expense::Expense,
            transaction::{Transaction, TransactionActor},
            transference::Transference,
        },
    },
};

/*************************** Test public objects *****************************/

#[cfg(any(test, feature = "test-helpers"))]
pub use self::{
    accounts::{
        add_account::mock::MockAddAccount, delete_account::mock::MockDeleteAccount,
        read_account::mock::MockReadAccount, update_account::mock::MockUpdateAccount,
    },
    cards::{
        add_card::mock::MockAddCard, delete_card::mock::MockDeleteCard,
        read_card::mock::MockReadCard, update_card::mock::MockUpdateCard,
    },
    transactions::{
        earning_add::mock::MockAddEarning, earning_delete::mock::MockDeleteEarning,
        earning_read::mock::MockReadEarning, earning_update::mock::MockUpdateEarning,
        expense_add::mock::MockAddExpense, expense_delete::mock::MockDeleteExpense,
        expense_read::mock::MockReadExpense, expense_update::mock::MockUpdateExpense,
        transaction_add::mock::MockAddTransaction, transaction_delete::mock::MockDeleteTransaction,
        transaction_read::mock::MockReadTransaction,
        transaction_update::mock::MockUpdateTransaction,
        transference_add::mock::MockAddTransference,
        transference_delete::mock::MockDeleteTransference,
        transference_read::mock::MockReadTransference,
        transference_update::mock::MockUpdateTransference,
    },
};
