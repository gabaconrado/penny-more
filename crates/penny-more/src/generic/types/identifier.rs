use std::{hash::Hash, marker::PhantomData};

use ulid::Ulid;

/// Centralized and strongly typed identifier
#[derive(Debug)]
pub struct Identifier<T> {
    /// The entity this identifier relates to
    _type: PhantomData<T>,
    /// The identifier value
    id: Ulid,
}

impl<T> Identifier<T> {
    /// Creates a new random [Identifier]
    pub fn random() -> Self {
        Self {
            _type: PhantomData,
            id: Ulid::new(),
        }
    }
}

impl<T> std::fmt::Display for Identifier<T> {
    /// Mimics the implementation of the internal data structure
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.id.to_string())
    }
}

impl<T> std::str::FromStr for Identifier<T> {
    type Err = ulid::DecodeError;
    /// Mimics the implementation of the internal data structure
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let id = Ulid::from_string(s)?;
        Ok(Identifier {
            _type: PhantomData,
            id,
        })
    }
}

impl<T> Clone for Identifier<T> {
    /// Mimics the implementation of the internal data structure, ignoring the type marker
    fn clone(&self) -> Self {
        Self {
            _type: PhantomData,
            id: self.id,
        }
    }
}
impl<T> Copy for Identifier<T> {}

impl<T> PartialEq for Identifier<T> {
    /// Mimics the implementation of the internal data structure, ignoring the type marker
    fn eq(&self, other: &Self) -> bool {
        self.id == other.id
    }
}
impl<T> Eq for Identifier<T> {}

impl<T> Hash for Identifier<T> {
    /// Mimics the implementation of the internal data structure, ignoring the type marker
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.id.hash(state);
    }
}

impl<T> PartialOrd for Identifier<T> {
    /// Mimics the implementation of the internal data structure, ignoring the type marker
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        self.id.partial_cmp(&other.id)
    }
}

impl<T> Ord for Identifier<T> {
    /// Mimics the implementation of the internal data structure, ignoring the type marker
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.id.cmp(&other.id)
    }
}

#[cfg(any(test, feature = "test-helpers"))]
impl<T> Identifier<T> {
    /// Creates a random value for testing
    pub fn sample() -> Self {
        Self::random()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    type Sut = Identifier<u8>;

    mod new {
        use super::*;

        #[test]
        fn should_create_random() {
            let n = 1000;
            let mut set = std::collections::HashSet::new();
            for _ in 0..n {
                let id = Sut::random();
                assert!(set.insert(id));
            }
        }
    }

    mod parse_str {
        use super::*;

        #[test]
        fn should_parse_to_string() {
            let id = Sut::sample();
            let expected = id.id.to_string();

            let parsed = id.to_string();

            assert_eq!(expected, parsed);
        }

        #[test]
        fn should_parse_from_string() {
            use std::str::FromStr;
            let value = Ulid::new();

            let parsed =
                Sut::from_str(&value.to_string()).expect("Could not parse identifier from string");

            assert_eq!(parsed.id, value);
        }

        #[test]
        fn should_error_if_invalid_string() {
            use std::str::FromStr;
            let value = "invalid";

            let res = Sut::from_str(value);

            assert!(matches!(res, Err(ulid::DecodeError::InvalidLength)));
        }
    }

    mod copy_clone {
        use super::*;

        #[test]
        fn should_support_copy_and_clone() {
            let id = Sut::sample();
            let cloned = id;
            let copied = id;

            assert_eq!(cloned, id);
            assert_eq!(copied, id);
        }
    }

    mod eq {
        use super::*;

        #[test]
        fn should_return_equals() {
            let id = Sut::sample();
            let cloned = id;

            assert_eq!(cloned, id);
        }

        #[test]
        fn should_return_different() {
            let id = Sut::sample();
            let other = Sut::sample();

            assert_ne!(id, other);
        }
    }

    mod ord {
        use super::*;

        #[test]
        fn should_order() {
            let ts_start = rand::random::<u64>();
            let id = Sut {
                _type: PhantomData,
                id: Ulid::from_parts(ts_start, rand::random()),
            };
            let sec_id = Sut {
                _type: PhantomData,
                id: Ulid::from_parts(ts_start + 1, rand::random()),
            };
            let third_id = Sut {
                _type: PhantomData,
                id: Ulid::from_parts(ts_start + 2, rand::random()),
            };

            assert!(id == id);
            assert!(id < sec_id);
            assert!(id < third_id);
            assert!(sec_id < third_id);
        }
    }
}
