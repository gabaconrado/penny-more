/// Identifier generic type
pub(crate) mod identifier;
/// Monetary value type
pub(crate) mod monetary_value;
