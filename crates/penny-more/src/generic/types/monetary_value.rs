use std::str::FromStr;

/// A type to represent money inside the system
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct MonetaryValue {
    /// Value in cents
    value: u64,
}

impl MonetaryValue {
    /// Creates a new [MonetaryValue]
    pub fn new(value: u64) -> Self {
        Self { value }
    }

    /// Creates a new [MonetaryValue] from its parts, units and cents
    pub fn from_parts(units: u64, cents: u8) -> Self {
        let units_cents = cents / 100;
        let cent = cents % 100;
        let unit = (units + u64::from(units_cents)) * 100;
        let value = unit + u64::from(cent);
        Self::new(value)
    }
}

impl FromStr for MonetaryValue {
    /// An string with the error message
    type Err = String;

    /// Tries to parse the number from a string format
    ///
    /// Valid format is <integer>.<cents>; Example: 99.00
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s.split('.').collect::<Vec<&str>>().as_slice() {
            // Integer format
            [units] => {
                // Parse split numeric values
                let units_int = u64::from_str(units).map_err(|err| err.to_string())?;
                let units_mul = 100;
                let value = units_int * units_mul;
                Ok(MonetaryValue::new(value))
            }
            // Float format
            [units, cents] => {
                // Parse split numeric values
                let units_int = u64::from_str(units).map_err(|err| err.to_string())?;
                let cents_int = u64::from_str(cents).map_err(|err| err.to_string())?;
                // Check length of cents
                if cents_int >= 100 {
                    return Err(format!("Cents part of value is too big: {s}"));
                }
                // Defines multipliers
                let units_mul = 100;
                let cents_mul = if cents_int < 10 { 10 } else { 1 };
                let value = (units_int * units_mul) + (cents_int * cents_mul);
                Ok(MonetaryValue::new(value))
            }
            _ => Err(format!("Invalid value to be parsed: {s}")),
        }
    }
}

impl std::fmt::Display for MonetaryValue {
    /// Writes the value in the format <integer>.<cents>
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let units = self.value / 100;
        let cents = self.value % 100;
        write!(f, "{units}.{cents}")
    }
}

#[cfg(any(test, feature = "test-helpers"))]
impl MonetaryValue {
    /// Creates a random value for testing
    pub fn sample() -> Self {
        let value = rand::random::<u8>().into();
        Self::new(value)
    }
}

#[cfg(test)]
mod tests {

    mod new {
        use super::super::*;

        #[test]
        fn should_create_new() {
            let value = rand::random::<u8>().into();

            let mv = MonetaryValue::new(value);

            assert_eq!(mv.value, value);
        }

        #[test]
        fn should_create_from_parts() {
            let values_expected = [
                ((10, 10), 1010),
                ((0, 10), 10),
                ((20, 1), 2001),
                ((1000, 100), 100100),
                ((9950, 250), 995250),
            ];
            for ((units, cents), expected) in values_expected {
                let mv = MonetaryValue::from_parts(units, cents);
                assert_eq!(mv.value, expected);
            }
        }
    }

    mod parse_str {
        use super::super::*;

        #[test]
        fn should_parse_from_string() -> Result<(), String> {
            let values_expected = [
                ("0", 0),
                ("1", 100),
                ("0.59", 59),
                ("1.28", 128),
                ("10.10", 1010),
                ("10.2", 1020),
                ("10.9", 1090),
                ("20", 2000),
                ("20.32", 2032),
                ("728.59", 72859),
                ("1029", 102900),
                ("1089.00", 108900),
                ("20000", 2000000),
                ("2627132.81", 262713281),
            ];
            for (value, expected) in values_expected {
                let mv = MonetaryValue::from_str(value)?;
                assert_eq!(mv.value, expected);
            }
            Ok(())
        }

        #[test]
        fn should_error_if_invalid_value() {
            let values = [
                "invalid", "-0.59", "1a.10", "10.1b", "1.100", "1,59", "10.10.10",
            ];
            for value in values {
                let res = MonetaryValue::from_str(value);
                assert!(res.is_err());
            }
        }
    }
}
