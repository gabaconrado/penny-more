use crate::Account;

/// Defines the behavior of an object that can add an Account
pub trait AddAccount {
    /// Adds a new account into the system
    fn add(&self, account: &Account) -> anyhow::Result<()>;
}

/// Manages the adding of a new account
///
/// Simply calls the underlying data storage to add the account
///
/// # Errors
///
/// - Internal error;
pub fn add_account_controller<A>(add_account: &A, account: &Account) -> anyhow::Result<()>
where
    A: AddAccount,
{
    add_account.add(account)?;
    Ok(())
}

#[cfg(any(test, feature = "test-helpers"))]
pub mod mock {
    use super::*;

    /// Mock [AddAccount] object useful for testing
    #[derive(Clone, Debug, Default)]
    pub struct MockAddAccount {
        /// Indicates function should return an error
        pub err: bool,
    }

    impl AddAccount for MockAddAccount {
        /// Default successful response
        ///
        /// # Error
        ///
        /// - Internal flag set;
        fn add(&self, _account: &Account) -> anyhow::Result<()> {
            if self.err {
                anyhow::bail!("MockAddAccount internal flag set")
            }
            Ok(())
        }
    }
}

#[cfg(test)]
mod tests {

    mod controller {
        use crate::MockAddAccount;

        use super::super::*;

        #[test]
        fn should_add_account() -> anyhow::Result<()> {
            let aa = MockAddAccount::default();
            let account = Account::sample();

            add_account_controller(&aa, &account)?;

            Ok(())
        }

        #[test]
        fn should_error_if_internal_flag_set() {
            let account = Account::sample();
            let aa = MockAddAccount { err: true };

            let res = add_account_controller(&aa, &account);

            assert!(res.is_err());
        }
    }
}
