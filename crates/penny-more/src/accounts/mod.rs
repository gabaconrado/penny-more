/// Account-related types
pub(crate) mod types;

/// All add-account-related functionality
pub(crate) mod add_account;
/// All delete-account-related functionality
pub(crate) mod delete_account;
/// All read-account-related functionality
pub(crate) mod read_account;
/// All update-account-related functionality
pub(crate) mod update_account;
