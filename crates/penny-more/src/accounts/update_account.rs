use crate::{Account, AccountId, MonetaryValue, ReadAccount};

/// Defines the behavior of an object that can update an Account
pub trait UpdateAccount {
    /// Updates an account in the system
    fn update(&self, account: &Account) -> anyhow::Result<()>;
}

/// Object that can be used to translate what should be updated in an [Account]
#[derive(Debug, Clone)]
pub struct AccountChangeSet {
    /// Account that will be changed
    pub id: AccountId,
    /// New name, [None] means to not update value
    pub name: Option<String>,
    /// New balance, [None] means to not update value
    pub balance: Option<MonetaryValue>,
}

impl AccountChangeSet {
    /// Creates a new [AccountChangeSet]
    pub fn new(id: AccountId, name: Option<String>, balance: Option<MonetaryValue>) -> Self {
        Self { id, name, balance }
    }
}

/// Manages the updating of an account
///
/// # Errors
///
/// - Account does not exist;
/// - Internal;
pub fn update_account_controller<R, U>(
    read_account: &R,
    upd_account: &U,
    account_cs: AccountChangeSet,
) -> anyhow::Result<()>
where
    U: UpdateAccount,
    R: ReadAccount,
{
    let saved_account = read_account.read(account_cs.id)?;

    let id = account_cs.id;
    let name = account_cs
        .name
        .unwrap_or_else(|| saved_account.name().to_string());
    let balance = account_cs
        .balance
        .unwrap_or_else(|| saved_account.balance());
    let account = Account::new(id, name, balance);

    upd_account.update(&account)?;

    Ok(())
}

#[cfg(any(test, feature = "test-helpers"))]
pub mod mock {
    use super::*;

    /// Mock [UpdateAccount] object useful for testing
    #[derive(Debug, Default)]
    pub struct MockUpdateAccount {
        /// Holds a clone of the last account object that was used in an update call
        pub expected_account: Option<Account>,
        /// Indicates function should return an error
        pub err: bool,
    }

    impl MockUpdateAccount {
        /// Sets an expected account object to be matched in the update method call
        ///
        /// Replaces any existing account that was added before
        pub fn set_expected_account(&mut self, account: &Account) {
            self.expected_account = Some(account.clone());
        }
    }

    impl UpdateAccount for MockUpdateAccount {
        /// Default successful response
        ///
        /// # Error
        ///
        /// - Internal error flag set;
        fn update(&self, account: &Account) -> anyhow::Result<()> {
            if self.err {
                anyhow::bail!("MockDeleteAccount internal error flag set");
            }
            if let Some(exp) = &self.expected_account {
                if exp != account {
                    anyhow::bail!("Passed account is different than expected");
                }
            }
            Ok(())
        }
    }
}

#[cfg(test)]
mod tests {

    mod account_change_set {
        use crate::test_helpers;

        use super::super::*;

        #[test]
        fn should_create_new() {
            let id = AccountId::sample();
            let name = Some(test_helpers::random_string(32));
            let balance = Some(MonetaryValue::sample());

            let acs = AccountChangeSet::new(id, name.clone(), balance);

            assert_eq!(acs.id, id);
            assert_eq!(acs.name, name);
            assert_eq!(acs.balance, balance);
        }
    }

    mod controller {
        use crate::{test_helpers, MockReadAccount, MockUpdateAccount};

        use super::super::*;

        #[test]
        fn should_update_account() -> anyhow::Result<()> {
            let account = Account::sample();
            let ra = MockReadAccount {
                accounts: vec![account.clone()],
                ..Default::default()
            };
            let mut ua = MockUpdateAccount::default();

            // Updating name
            let new_name = test_helpers::random_string(32);
            let account_cs = AccountChangeSet::new(account.id(), Some(new_name.clone()), None);
            let expected_account = Account::new(account.id(), new_name, account.balance());
            ua.set_expected_account(&expected_account);
            update_account_controller(&ra, &ua, account_cs)?;

            // Updating balance
            let new_balance = MonetaryValue::sample();
            let account_cs = AccountChangeSet::new(account.id(), None, Some(new_balance));
            let expected_account =
                Account::new(account.id(), account.name().to_string(), new_balance);
            ua.set_expected_account(&expected_account);
            update_account_controller(&ra, &ua, account_cs)?;

            // Updating nothing
            let account_cs = AccountChangeSet::new(account.id(), None, None);
            let expected_account = account;
            ua.set_expected_account(&expected_account);
            update_account_controller(&ra, &ua, account_cs)?;

            Ok(())
        }

        #[test]
        fn should_error_if_update_fails() {
            let ra = MockReadAccount::default();
            let ua = MockUpdateAccount {
                err: true,
                ..Default::default()
            };
            let account = Account::sample();
            let account_cs = AccountChangeSet::new(account.id(), None, None);

            let res = update_account_controller(&ra, &ua, account_cs);

            assert!(res.is_err());
        }

        #[test]
        fn should_error_if_read_fails() {
            let ra = MockReadAccount {
                err: true,
                ..Default::default()
            };
            let ua = MockUpdateAccount::default();
            let account = Account::sample();
            let account_cs = AccountChangeSet::new(account.id(), None, None);

            let res = update_account_controller(&ra, &ua, account_cs);

            assert!(res.is_err());
        }
    }
}
