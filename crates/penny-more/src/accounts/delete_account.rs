use crate::AccountId;

/// Defines the behavior of an object that can delete an Account
pub trait DeleteAccount {
    /// Deletes an account from the system
    fn delete(&self, account_id: AccountId) -> anyhow::Result<()>;
}

/// Manages the deletion of an account
///
/// # Errors
///
/// - Internal;
pub fn delete_account_controller<D>(del_account: &D, account_id: AccountId) -> anyhow::Result<()>
where
    D: DeleteAccount,
{
    del_account.delete(account_id)?;
    Ok(())
}

#[cfg(any(test, feature = "test-helpers"))]
pub mod mock {
    use super::*;

    /// Mock [DeleteAccount] object useful for testing
    #[derive(Clone, Debug, Default)]
    pub struct MockDeleteAccount {
        /// Indicates function should return an error
        pub err: bool,
    }

    impl DeleteAccount for MockDeleteAccount {
        /// Default successful response
        ///
        /// # Error
        ///
        /// - Internal error flag set;
        fn delete(&self, _account_id: AccountId) -> anyhow::Result<()> {
            if self.err {
                anyhow::bail!("MockDeleteAccount internal error flag set");
            }
            Ok(())
        }
    }
}

#[cfg(test)]
mod tests {

    mod controller {
        use crate::MockDeleteAccount;

        use super::super::*;

        #[test]
        fn should_delete_account() -> anyhow::Result<()> {
            let da = MockDeleteAccount::default();
            let account_id = AccountId::sample();

            delete_account_controller(&da, account_id)?;

            Ok(())
        }

        #[test]
        fn should_error_if_internal_flag_set() {
            let da = MockDeleteAccount {
                err: true,
                ..Default::default()
            };
            let account_id = AccountId::sample();

            let res = delete_account_controller(&da, account_id);

            assert!(res.is_err());
        }
    }
}
