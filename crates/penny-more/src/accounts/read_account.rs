use crate::{Account, AccountId};

/// Defines the behavior of an object that can read stored Accounts
pub trait ReadAccount {
    /// Reads all saved accounts
    fn read_all(&self) -> anyhow::Result<Vec<Account>>;

    /// Reads an account by its ID
    ///
    /// # Errors
    ///
    /// - Account does not exist;
    fn read(&self, id: AccountId) -> anyhow::Result<Account>;
}

/// Manages the listing of all accounts
pub fn list_accounts_controller<R>(read_account: &R) -> anyhow::Result<Vec<Account>>
where
    R: ReadAccount,
{
    let accounts = read_account.read_all()?;
    Ok(accounts)
}

#[cfg(any(test, feature = "test-helpers"))]
pub mod mock {
    use crate::{test_helpers, MonetaryValue};

    use super::*;

    /// Mock [ReadAccount] object useful for tests
    #[derive(Debug, Clone, Default)]
    pub struct MockReadAccount {
        /// Indicates if the functions should return errors
        pub err: bool,
        /// The array of accounts to be returned by the methods
        pub accounts: Vec<Account>,
    }

    impl ReadAccount for MockReadAccount {
        /// Returns the internal vector of accounts
        ///
        /// # Errors
        ///
        /// - Internal flag set;
        fn read_all(&self) -> anyhow::Result<Vec<Account>> {
            if self.err {
                anyhow::bail!("MockReadAccount internal error flag set");
            }
            Ok(self.accounts.clone())
        }

        /// Searches the internal vector for the requested account and returns it
        ///
        /// Returns a random [Account] object with the requested id if it is not
        /// in the internal vector
        ///
        /// # Errors
        ///
        /// - Internal flag set;
        fn read(&self, id: AccountId) -> anyhow::Result<Account> {
            if self.err {
                anyhow::bail!("MockReadAccount internal error flag set");
            }
            let acc = self
                .accounts
                .iter()
                .cloned()
                .find(|a| a.id() == id)
                .unwrap_or_else(|| {
                    Account::new(id, test_helpers::random_string(32), MonetaryValue::sample())
                });
            Ok(acc)
        }
    }
}

#[cfg(test)]
mod tests {
    mod controller {
        use crate::MockReadAccount;

        use super::super::*;

        #[test]
        fn should_list_accounts() -> anyhow::Result<()> {
            let expected = vec![Account::sample(), Account::sample()];
            let ra = MockReadAccount {
                accounts: expected.clone(),
                ..Default::default()
            };

            let accounts = list_accounts_controller(&ra)?;

            assert_eq!(expected, accounts);
            Ok(())
        }

        #[test]
        fn should_error_if_internal_flag_set() {
            let ra = MockReadAccount {
                err: true,
                ..Default::default()
            };

            let res = list_accounts_controller(&ra);

            assert!(res.is_err());
        }
    }
}
