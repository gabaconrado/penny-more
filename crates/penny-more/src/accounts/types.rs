use crate::{AccountId, MonetaryValue};

/// Object to represent an Account in the system
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Account {
    /// Account identifier
    id: AccountId,
    /// The name of the account
    name: String,
    /// The initial balance of the account in cents
    balance: MonetaryValue,
}

impl Account {
    /// Creates a new [Account]
    pub fn new(id: AccountId, name: String, balance: MonetaryValue) -> Self {
        Self { id, name, balance }
    }

    /// Identifier getter
    pub fn id(&self) -> AccountId {
        self.id
    }

    /// Name getter
    pub fn name(&self) -> &str {
        self.name.as_ref()
    }

    /// Balance getter
    pub fn balance(&self) -> MonetaryValue {
        self.balance
    }
}

#[cfg(any(test, feature = "test-helpers"))]
impl Account {
    /// Creates a random value for testing
    pub fn sample() -> Self {
        let id = AccountId::sample();
        let name = crate::test_helpers::random_string(16);
        let balance = MonetaryValue::sample();
        Account::new(id, name, balance)
    }
}

#[cfg(test)]
mod tests {

    mod new {
        use super::super::*;

        #[test]
        fn should_create_new() {
            let id = AccountId::sample();
            let name = crate::test_helpers::random_string(16);
            let balance = MonetaryValue::sample();
            let account = Account::new(id, name.clone(), balance);

            assert_eq!(account.id, id);
            assert_eq!(account.name, name);
            assert_eq!(account.balance, balance);
        }

        #[test]
        fn should_return_getters() {
            let account = Account::sample();

            assert_eq!(account.id, account.id());
            assert_eq!(account.name, *account.name());
            assert_eq!(account.balance, account.balance());
        }
    }
}
