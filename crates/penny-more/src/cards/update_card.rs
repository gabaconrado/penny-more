use crate::{AccountId, Card, CardId, MonetaryValue, ReadAccount, ReadCard};

/// Defines the behavior of an object that can update a Card
pub trait UpdateCard {
    /// Updates a card in the system
    fn update(&self, card: &Card) -> anyhow::Result<()>;
}

/// Object that can be used to translate what should be updated in a [Card]
#[derive(Debug, Clone)]
pub struct CardChangeSet {
    /// Card that will be changed
    pub id: CardId,
    /// New name, [None] means to not update value
    pub name: Option<String>,
    /// New limit, [None] means to not update value
    pub limit: Option<MonetaryValue>,
    /// New related account, [None] means to not update value
    pub account: Option<Option<AccountId>>,
}

impl CardChangeSet {
    /// Creates a new [CardChangeSet]
    pub fn new(
        id: CardId,
        name: Option<String>,
        limit: Option<MonetaryValue>,
        account: Option<Option<AccountId>>,
    ) -> Self {
        Self {
            id,
            name,
            limit,
            account,
        }
    }
}

/// Manages the updating of an card
///
/// # Errors
///
/// - Card does not exist;
/// - Internal;
pub fn update_card_controller<R, U, A>(
    read_card: &R,
    upd_card: &U,
    read_acc: &A,
    card_cs: CardChangeSet,
) -> anyhow::Result<()>
where
    U: UpdateCard,
    R: ReadCard,
    A: ReadAccount,
{
    // Checks if objects exist
    let saved_card = read_card.read(card_cs.id)?;
    if let Some(Some(new_acc_id)) = card_cs.account {
        let _account = read_acc.read(new_acc_id)?;
    }

    let id = card_cs.id;
    let name = card_cs
        .name
        .unwrap_or_else(|| saved_card.name().to_string());
    let balance = card_cs.limit.unwrap_or_else(|| saved_card.limit());
    let account = card_cs.account.unwrap_or_else(|| saved_card.account());
    let card = Card::new(id, name, balance, account);

    upd_card.update(&card)?;

    Ok(())
}

#[cfg(any(test, feature = "test-helpers"))]
pub mod mock {
    use super::*;

    /// Mock [UpdateCard] object useful for testing
    #[derive(Debug, Default)]
    pub struct MockUpdateCard {
        /// Holds a clone of the last card object that was used in an update call
        pub expected_card: Option<Card>,
        /// Indicates function should return an error
        pub err: bool,
    }

    impl MockUpdateCard {
        /// Sets an expected card object to be matched in the update method call
        ///
        /// Replaces any existing card that was added before
        pub fn set_expected_card(&mut self, card: &Card) {
            self.expected_card = Some(card.clone());
        }
    }

    impl UpdateCard for MockUpdateCard {
        /// Default successful response
        ///
        /// # Error
        ///
        /// - Internal error flag set;
        fn update(&self, card: &Card) -> anyhow::Result<()> {
            if self.err {
                anyhow::bail!("MockDeleteCard internal error flag set");
            }
            if let Some(exp) = &self.expected_card {
                if exp != card {
                    anyhow::bail!("Passed card is different than expected");
                }
            }
            Ok(())
        }
    }
}

#[cfg(test)]
mod tests {

    mod card_change_set {
        use crate::test_helpers;

        use super::super::*;

        #[test]
        fn should_create_new() {
            let id = CardId::sample();
            let name = Some(test_helpers::random_string(32));
            let limit = Some(MonetaryValue::sample());
            let account = Some(Some(AccountId::sample()));

            let acs = CardChangeSet::new(id, name.clone(), limit, account);

            assert_eq!(acs.id, id);
            assert_eq!(acs.name, name);
            assert_eq!(acs.limit, limit);
            assert_eq!(acs.account, account);
        }
    }

    mod controller {
        use crate::{test_helpers, Account, MockReadAccount, MockReadCard, MockUpdateCard};

        use super::super::*;

        #[test]
        fn should_update_card() -> anyhow::Result<()> {
            let card = Card::sample();
            let rc = MockReadCard {
                cards: vec![card.clone()],
                ..Default::default()
            };
            let ra = MockReadAccount::default();
            let mut ua = MockUpdateCard::default();

            // Updating name
            let new_name = test_helpers::random_string(32);
            let card_cs = CardChangeSet::new(card.id(), Some(new_name.clone()), None, None);
            let expected_card = Card::new(card.id(), new_name, card.limit(), card.account());
            ua.set_expected_card(&expected_card);
            update_card_controller(&rc, &ua, &ra, card_cs)?;

            // Updating balance
            let new_limit = MonetaryValue::sample();
            let card_cs = CardChangeSet::new(card.id(), None, Some(new_limit), None);
            let expected_card = Card::new(
                card.id(),
                card.name().to_string(),
                new_limit,
                card.account(),
            );
            ua.set_expected_card(&expected_card);
            update_card_controller(&rc, &ua, &ra, card_cs)?;

            // Updating account
            let new_account = Account::sample();
            let card_cs = CardChangeSet::new(card.id(), None, None, Some(Some(new_account.id())));
            let expected_card = Card::new(
                card.id(),
                card.name().to_string(),
                card.limit(),
                Some(new_account.id()),
            );
            ua.set_expected_card(&expected_card);
            update_card_controller(&rc, &ua, &ra, card_cs)?;

            // Updating nothing
            let card_cs = CardChangeSet::new(card.id(), None, None, None);
            let expected_card = card;
            ua.set_expected_card(&expected_card);
            update_card_controller(&rc, &ua, &ra, card_cs)?;

            Ok(())
        }

        #[test]
        fn should_not_read_account_if_not_updated() -> anyhow::Result<()> {
            let card = Card::sample();
            let rc = MockReadCard::default();
            let ra = MockReadAccount {
                err: true,
                ..Default::default()
            };
            let ua = MockUpdateCard::default();

            let card_cs = CardChangeSet::new(card.id(), None, None, None);
            update_card_controller(&rc, &ua, &ra, card_cs)?;

            Ok(())
        }

        #[test]
        fn should_error_if_update_card_fails() {
            let ra = MockReadAccount::default();
            let rc = MockReadCard::default();
            let ua = MockUpdateCard {
                err: true,
                ..Default::default()
            };
            let card = Card::sample();
            let card_cs = CardChangeSet::new(card.id(), None, None, None);

            let res = update_card_controller(&rc, &ua, &ra, card_cs);

            assert!(res.is_err());
        }

        #[test]
        fn should_error_if_read_card_fails() {
            let ra = MockReadAccount::default();
            let rc = MockReadCard {
                err: true,
                ..Default::default()
            };
            let ua = MockUpdateCard::default();
            let card = Card::sample();
            let card_cs = CardChangeSet::new(card.id(), None, None, None);

            let res = update_card_controller(&rc, &ua, &ra, card_cs);

            assert!(res.is_err());
        }

        #[test]
        fn should_error_if_read_account_fails() {
            let ra = MockReadAccount {
                err: true,
                ..Default::default()
            };
            let rc = MockReadCard::default();
            let ua = MockUpdateCard::default();
            let card = Card::sample();
            let card_cs =
                CardChangeSet::new(card.id(), None, None, Some(Some(AccountId::sample())));

            let res = update_card_controller(&rc, &ua, &ra, card_cs);

            assert!(res.is_err());
        }
    }
}
