use crate::{AccountId, CardId, MonetaryValue};

/// Object to represent a Card in the system
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Card {
    /// Card identifier
    id: CardId,
    /// The name of the card
    name: String,
    /// The monthly limit of the card
    limit: MonetaryValue,
    /// An optional related account to the card
    account: Option<AccountId>,
}

impl Card {
    /// Creates a new [Card] object
    pub fn new(id: CardId, name: String, limit: MonetaryValue, account: Option<AccountId>) -> Self {
        Self {
            id,
            name,
            limit,
            account,
        }
    }

    /// Identifier getter
    pub fn id(&self) -> CardId {
        self.id
    }

    /// Name getter
    pub fn name(&self) -> &str {
        self.name.as_ref()
    }

    /// Limit getter
    pub fn limit(&self) -> MonetaryValue {
        self.limit
    }

    /// Account getter
    pub fn account(&self) -> Option<AccountId> {
        self.account
    }
}

#[cfg(any(test, feature = "test-helpers"))]
impl Card {
    /// Creates a random value for testing
    pub fn sample() -> Self {
        let id = CardId::sample();
        let name = crate::test_helpers::random_string(16);
        let limit = MonetaryValue::sample();
        Card::new(id, name, limit, None)
    }

    /// Creates a random value for testing, accepts parameters
    pub fn parameterized(with_account: bool) -> Self {
        let mut card = Self::sample();
        if with_account {
            card.account = Some(AccountId::sample());
        }
        card
    }

    /// Sets the account of the card
    pub fn set_account(&mut self, account: AccountId) {
        self.account = Some(account)
    }
}

#[cfg(test)]
mod tests {

    mod new {
        use super::super::*;

        #[test]
        fn should_create_new() {
            let id = CardId::sample();
            let name = crate::test_helpers::random_string(16);
            let limit = MonetaryValue::sample();
            let account = Some(AccountId::sample());
            let card = Card::new(id, name.clone(), limit, account);

            assert_eq!(card.id, id);
            assert_eq!(card.name, name);
            assert_eq!(card.limit, limit);
            assert_eq!(card.account, account);
        }

        #[test]
        fn should_return_getters() {
            let card = Card::sample();

            assert_eq!(card.id, card.id());
            assert_eq!(card.name, card.name());
            assert_eq!(card.limit, card.limit());
            assert_eq!(card.account, card.account());
        }
    }
}
