use crate::{Card, ReadAccount};

/// Defines the behavior of an object that can add a Card
pub trait AddCard {
    /// Adds a new card into the system
    fn add(&self, card: &Card) -> anyhow::Result<()>;
}

/// Manages the adding of a new card
///
/// Simply calls the underlying data storage to add the card
///
/// # Errors
///
/// - The related account with the card does not exist;
/// - Internal error;
pub fn add_card_controller<A, R>(add_card: &A, read_acc: &R, card: &Card) -> anyhow::Result<()>
where
    A: AddCard,
    R: ReadAccount,
{
    if let Some(account_id) = card.account() {
        // Will error if account does not exist
        let _account = read_acc.read(account_id)?;
    }
    add_card.add(card)?;
    Ok(())
}

#[cfg(any(test, feature = "test-helpers"))]
pub mod mock {
    use super::*;

    /// Mock [AddCard] object useful for testing
    #[derive(Clone, Debug, Default)]
    pub struct MockAddCard {
        /// Indicates function should return an error
        pub err: bool,
    }

    impl AddCard for MockAddCard {
        /// Default successful response
        ///
        /// # Error
        ///
        /// - Internal flag set;
        fn add(&self, _card: &Card) -> anyhow::Result<()> {
            if self.err {
                anyhow::bail!("MockAddCard internal flag set")
            }
            Ok(())
        }
    }
}

#[cfg(test)]
mod tests {

    mod controller {
        use crate::{MockAddCard, MockReadAccount};

        use super::super::*;

        #[test]
        fn should_add_card() -> anyhow::Result<()> {
            let ac = MockAddCard::default();
            let ra = MockReadAccount::default();
            let card = Card::sample();

            add_card_controller(&ac, &ra, &card)?;

            Ok(())
        }

        #[test]
        fn should_not_check_account_if_no_related_account() -> anyhow::Result<()> {
            let ac = MockAddCard::default();
            let ra = MockReadAccount {
                err: true,
                ..Default::default()
            };
            let card = Card::sample();

            add_card_controller(&ac, &ra, &card)?;

            Ok(())
        }

        #[test]
        fn should_error_if_add_card_fails() {
            let card = Card::sample();
            let ra = MockReadAccount::default();
            let ca = MockAddCard {
                err: true,
                ..Default::default()
            };

            let res = add_card_controller(&ca, &ra, &card);

            assert!(res.is_err());
        }

        #[test]
        fn should_error_if_read_account_fails() {
            let card = Card::parameterized(true);
            let ra = MockReadAccount {
                err: true,
                ..Default::default()
            };
            let ca = MockAddCard::default();

            let res = add_card_controller(&ca, &ra, &card);

            assert!(res.is_err());
        }
    }
}
