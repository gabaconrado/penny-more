use crate::CardId;

/// Defines the behavior of an object that can delete a Card
pub trait DeleteCard {
    /// Deletes an card from the system
    fn delete(&self, card_id: CardId) -> anyhow::Result<()>;
}

/// Manages the deletion of a card
///
/// # Errors
///
/// - Internal
pub fn delete_card_controller<D>(del_card: &D, card_id: CardId) -> anyhow::Result<()>
where
    D: DeleteCard,
{
    del_card.delete(card_id)?;
    Ok(())
}

#[cfg(any(test, feature = "test-helpers"))]
pub mod mock {
    use super::*;

    /// Mock [DeleteCard] object useful for testing
    #[derive(Clone, Debug, Default)]
    pub struct MockDeleteCard {
        /// Indicates function should return an error
        pub err: bool,
    }

    impl DeleteCard for MockDeleteCard {
        /// Default successful response
        ///
        /// # Error
        ///
        /// - Internal error flag set;
        fn delete(&self, _card_id: CardId) -> anyhow::Result<()> {
            if self.err {
                anyhow::bail!("MockDeleteCard internal error flag set");
            }
            Ok(())
        }
    }
}

#[cfg(test)]
mod tests {

    mod controller {
        use crate::MockDeleteCard;

        use super::super::*;

        #[test]
        fn should_delete_card() -> anyhow::Result<()> {
            let dc = MockDeleteCard::default();
            let card_id = CardId::sample();

            delete_card_controller(&dc, card_id)?;

            Ok(())
        }

        #[test]
        fn should_error_if_internal_flag_set() {
            let dc = MockDeleteCard {
                err: true,
                ..Default::default()
            };
            let card_id = CardId::sample();

            let res = delete_card_controller(&dc, card_id);

            assert!(res.is_err());
        }
    }
}
