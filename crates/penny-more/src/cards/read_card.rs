use crate::{Card, CardId};

/// Defines the behavior of an object that can read stored Cards
pub trait ReadCard {
    /// Reads all saved cards
    fn read_all(&self) -> anyhow::Result<Vec<Card>>;

    /// Reads a card by its ID
    ///
    /// # Errors
    ///
    /// - Card does not exist;
    fn read(&self, id: CardId) -> anyhow::Result<Card>;
}

/// Manages the listing of all cards
pub fn list_cards_controller<R>(read_card: &R) -> anyhow::Result<Vec<Card>>
where
    R: ReadCard,
{
    let cards = read_card.read_all()?;
    Ok(cards)
}

#[cfg(any(test, feature = "test-helpers"))]
pub mod mock {
    use crate::{test_helpers, MonetaryValue};

    use super::*;

    /// Mock [ReadCard] object useful for tests
    #[derive(Debug, Clone, Default)]
    pub struct MockReadCard {
        /// Indicates if the functions should return errors
        pub err: bool,
        /// The array of cards to be returned by the methods
        pub cards: Vec<Card>,
    }

    impl ReadCard for MockReadCard {
        /// Returns the internal vector of cards
        ///
        /// # Errors
        ///
        /// - Internal flag set;
        fn read_all(&self) -> anyhow::Result<Vec<Card>> {
            if self.err {
                anyhow::bail!("MockReadCard internal error flag set");
            }
            Ok(self.cards.clone())
        }

        /// Searches the internal vector for the requested card and returns it
        ///
        /// Returns a random [Card] object with the requested id if it is not
        /// in the internal vector
        ///
        /// # Errors
        ///
        /// - Internal flag set;
        fn read(&self, id: CardId) -> anyhow::Result<Card> {
            if self.err {
                anyhow::bail!("MockReadCard internal error flag set");
            }
            let card = self
                .cards
                .iter()
                .cloned()
                .find(|a| a.id() == id)
                .unwrap_or_else(|| {
                    Card::new(
                        id,
                        test_helpers::random_string(32),
                        MonetaryValue::sample(),
                        None,
                    )
                });
            Ok(card)
        }
    }
}

#[cfg(test)]
mod tests {
    mod controller {
        use crate::MockReadCard;

        use super::super::*;

        #[test]
        fn should_list_cards() -> anyhow::Result<()> {
            let expected = vec![Card::sample(), Card::sample()];
            let rc = MockReadCard {
                cards: expected.clone(),
                ..Default::default()
            };

            let cards = list_cards_controller(&rc)?;

            assert_eq!(expected, cards);
            Ok(())
        }

        #[test]
        fn should_error_if_internal_flag_set() {
            let rc = MockReadCard {
                err: true,
                ..Default::default()
            };

            let res = list_cards_controller(&rc);

            assert!(res.is_err());
        }
    }
}
