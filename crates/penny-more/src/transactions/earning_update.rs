use chrono::{DateTime, Utc};

use crate::{
    Earning, MonetaryValue, ReadEarning, Transaction, TransactionActor, TransactionId,
    UpdateTransaction,
};

/// Defines the behavior of an object that can update a Transaction
pub trait UpdateEarning: UpdateTransaction {
    /// Updates a transaction in the system
    ///
    /// # Errors
    ///
    /// - Internal;
    fn update(&self, earning: &Earning) -> anyhow::Result<()> {
        UpdateTransaction::update(self, earning.transaction())
    }
}

/// Object that can be used to translate what should be updated in a [Earning]
#[derive(Debug, Clone)]
pub struct EarningChangeSet {
    /// Earning that will be changed
    pub id: TransactionId,
    /// New timestamp, [None] means to not update value
    pub timestamp: Option<DateTime<Utc>>,
    /// New owner, [None] means to not update value
    pub owner: Option<TransactionActor>,
    /// New amount, [None] means to not update value
    pub amount: Option<MonetaryValue>,
    /// New description, [None] means to not update value
    pub description: Option<Option<String>>,
}

impl EarningChangeSet {
    /// Creates a new [EarningChangeSet]
    pub fn new(
        id: TransactionId,
        timestamp: Option<DateTime<Utc>>,
        owner: Option<TransactionActor>,
        amount: Option<MonetaryValue>,
        description: Option<Option<String>>,
    ) -> Self {
        Self {
            id,
            timestamp,
            owner,
            amount,
            description,
        }
    }
}

/// Manages the updating of an earning
///
/// # Errors
///
/// - Earning does not exist;
/// - Internal;
pub fn update_earning_controller<R, U>(
    read_earning: &R,
    upd_earning: &U,
    earning_cs: EarningChangeSet,
) -> anyhow::Result<()>
where
    U: UpdateEarning,
    R: ReadEarning,
{
    let saved_earning = ReadEarning::read(read_earning, earning_cs.id)?;

    let id = earning_cs.id;
    let timestamp = earning_cs
        .timestamp
        .unwrap_or_else(|| saved_earning.timestamp());
    let owner = earning_cs.owner.unwrap_or_else(|| saved_earning.owner());
    let amount = earning_cs.amount.unwrap_or_else(|| saved_earning.amount());
    let description = earning_cs
        .description
        .unwrap_or_else(|| saved_earning.description().cloned());
    let earning = Earning::new(id, timestamp, owner, amount, description);

    UpdateEarning::update(upd_earning, &earning)?;

    Ok(())
}

#[cfg(any(test, feature = "test-helpers"))]
pub mod mock {
    use super::*;

    /// Mock [UpdateEarning] object useful for testing
    #[derive(Debug, Default)]
    pub struct MockUpdateEarning {
        /// Holds a clone of the last earning object that was used in an update call
        pub expected_earning: Option<Earning>,
        /// Indicates function should return an error
        pub err: bool,
    }

    impl MockUpdateEarning {
        /// Sets an expected transaction object to be matched in the update method call
        ///
        /// Replaces any existing transaction that was added before
        pub fn set_expected_earning(&mut self, earning: &Earning) {
            self.expected_earning = Some(earning.clone());
        }
    }

    impl UpdateTransaction for MockUpdateEarning {
        /// Default successful response
        ///
        /// # Error
        ///
        /// - Internal error flag set;
        fn update(&self, transaction: &Transaction) -> anyhow::Result<()> {
            if self.err {
                anyhow::bail!("MockUpdateEarning internal error flag set");
            }
            if let Some(e) = &self.expected_earning {
                if e.transaction() != transaction {
                    anyhow::bail!("Passed earning is different than expected");
                }
            }
            Ok(())
        }
    }

    impl UpdateEarning for MockUpdateEarning {}
}

#[cfg(test)]
mod tests {

    mod earning_change_set {
        use crate::test_helpers;

        use super::super::*;

        #[test]
        fn should_create_new() {
            let id = TransactionId::sample();
            let timestamp = Some(Utc::now());
            let owner = Some(TransactionActor::sample());
            let amount = Some(MonetaryValue::sample());
            let description = Some(Some(test_helpers::random_string(16)));

            let ecs = EarningChangeSet::new(id, timestamp, owner, amount, description.clone());

            assert_eq!(ecs.id, id);
            assert_eq!(ecs.timestamp, timestamp);
            assert_eq!(ecs.owner, owner);
            assert_eq!(ecs.amount, amount);
            assert_eq!(ecs.description, description);
        }
    }

    mod controller {
        use crate::{test_helpers, MockReadEarning, MockUpdateEarning};

        use super::super::*;

        #[test]
        fn should_update_earning() -> anyhow::Result<()> {
            let earning = Earning::sample();
            let re = MockReadEarning {
                earnings: vec![earning.clone()],
                ..Default::default()
            };
            let mut ue = MockUpdateEarning::default();

            // Updating timestamp
            let new_timestamp = Utc::now();
            let earning_cs =
                EarningChangeSet::new(earning.id(), Some(new_timestamp), None, None, None);
            let expected_earning = Earning::new(
                earning.id(),
                new_timestamp,
                earning.owner(),
                earning.amount(),
                earning.description().cloned(),
            );
            ue.set_expected_earning(&expected_earning);
            update_earning_controller(&re, &ue, earning_cs)?;

            // Updating owner
            let new_owner = TransactionActor::sample();
            let earning_cs = EarningChangeSet::new(earning.id(), None, Some(new_owner), None, None);
            let expected_earning = Earning::new(
                earning.id(),
                earning.timestamp(),
                new_owner,
                earning.amount(),
                earning.description().cloned(),
            );
            ue.set_expected_earning(&expected_earning);
            update_earning_controller(&re, &ue, earning_cs)?;

            // Updating amount
            let new_amount = MonetaryValue::sample();
            let earning_cs =
                EarningChangeSet::new(earning.id(), None, None, Some(new_amount), None);
            let expected_earning = Earning::new(
                earning.id(),
                earning.timestamp(),
                earning.owner(),
                new_amount,
                earning.description().cloned(),
            );
            ue.set_expected_earning(&expected_earning);
            update_earning_controller(&re, &ue, earning_cs)?;

            // Updating description
            let new_description = Some(test_helpers::random_string(16));
            let earning_cs = EarningChangeSet::new(
                earning.id(),
                None,
                None,
                None,
                Some(new_description.clone()),
            );
            let expected_earning = Earning::new(
                earning.id(),
                earning.timestamp(),
                earning.owner(),
                earning.amount(),
                new_description,
            );
            ue.set_expected_earning(&expected_earning);
            update_earning_controller(&re, &ue, earning_cs)?;

            // Updating nothing
            let earning_cs = EarningChangeSet::new(earning.id(), None, None, None, None);
            let expected_earning = earning;
            ue.set_expected_earning(&expected_earning);
            update_earning_controller(&re, &ue, earning_cs)?;

            Ok(())
        }

        #[test]
        fn should_error_if_update_fails() {
            let re = MockReadEarning::default();
            let ue = MockUpdateEarning {
                err: true,
                ..Default::default()
            };
            let earning = Transaction::sample();
            let earning_cs = EarningChangeSet::new(earning.id(), None, None, None, None);

            let res = update_earning_controller(&re, &ue, earning_cs);

            assert!(res.is_err());
        }

        #[test]
        fn should_error_if_read_fails() {
            let re = MockReadEarning {
                err: true,
                ..Default::default()
            };
            let ue = MockUpdateEarning::default();
            let earning = Earning::sample();
            let earning_cs = EarningChangeSet::new(earning.id(), None, None, None, None);

            let res = update_earning_controller(&re, &ue, earning_cs);

            assert!(res.is_err());
        }
    }
}
