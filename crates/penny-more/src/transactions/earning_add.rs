use crate::{AddTransaction, Earning};

/// Defines the behavior of an object that can add an Earning
pub trait AddEarning: AddTransaction {
    /// Adds a new earning into the system
    ///
    /// Default implementation calls the [AddTransaction] implementation
    fn add(&self, earning: &Earning) -> anyhow::Result<()> {
        AddTransaction::add(self, earning.transaction())
    }
}

/// Manages the adding of a new earning
///
/// Simply calls the underlying data storage to add the earning
///
/// # Errors
///
/// - Internal;
pub fn add_earning_controller<A>(add_earning: &A, earning: &Earning) -> anyhow::Result<()>
where
    A: AddEarning,
{
    AddEarning::add(add_earning, earning)?;
    Ok(())
}

#[cfg(any(test, feature = "test-helpers"))]
pub mod mock {
    use crate::Transaction;

    use super::*;

    /// Mock [AddEarning] object useful for testing
    #[derive(Clone, Debug, Default)]
    pub struct MockAddEarning {
        /// Indicates function should return an error
        pub err: bool,
    }

    impl AddTransaction for MockAddEarning {
        /// Default successful response
        ///
        /// # Error
        ///
        /// - Internal flag set;
        fn add(&self, _transaction: &Transaction) -> anyhow::Result<()> {
            if self.err {
                anyhow::bail!("MockAddEarning internal flag set")
            }
            Ok(())
        }
    }

    impl AddEarning for MockAddEarning {}
}

#[cfg(test)]
mod tests {

    mod controller {
        use crate::MockAddEarning;

        use super::super::*;

        #[test]
        fn should_add_earning() -> anyhow::Result<()> {
            let ae = MockAddEarning::default();
            let earning = Earning::sample();

            add_earning_controller(&ae, &earning)?;

            Ok(())
        }

        #[test]
        fn should_error_if_internal_flag_set() {
            let earning = Earning::sample();
            let ae = MockAddEarning { err: true };

            let res = add_earning_controller(&ae, &earning);

            assert!(res.is_err());
        }
    }
}
