use chrono::{DateTime, Utc};

use crate::{MonetaryValue, Transaction, TransactionActor, TransactionId};

use super::transaction::TransactionType;

#[derive(Debug, Clone, PartialEq, Eq)]
/// Represents an expense in the system
pub struct Expense {
    /// The internal transaction of the earning
    transaction: Transaction,
}

impl Expense {
    /// Creates a new [Expense]
    pub fn new(
        id: TransactionId,
        timestamp: DateTime<Utc>,
        owner: TransactionActor,
        amount: MonetaryValue,
        description: Option<String>,
    ) -> Self {
        let transaction = Transaction::new(
            id,
            timestamp,
            owner,
            amount,
            description,
            TransactionType::Expense,
        );
        Expense { transaction }
    }

    /// Internal transaction getter
    pub fn transaction(&self) -> &Transaction {
        &self.transaction
    }

    // Delegates all methods of the internal transaction
    delegate::delegate! {
        to self.transaction {
            /// Identifier getter
            pub fn id(&self) -> TransactionId;
            /// Timestamp getter
            pub fn timestamp(&self) -> DateTime<Utc>;
            /// Transaction owner getter
            pub fn owner(&self) -> TransactionActor;
            /// Amount getter
            pub fn amount(&self) -> MonetaryValue;
            /// Description getter
            pub fn description(&self) -> Option<&String>;
        }
    }
}

impl TryFrom<Transaction> for Expense {
    type Error = anyhow::Error;
    /// Creates a new [Expense] with the inner transference
    ///
    /// # Errors
    ///
    /// - The transaction is not from the correct type
    fn try_from(transaction: Transaction) -> Result<Self, Self::Error> {
        match transaction.transaction_type() {
            TransactionType::Expense => Ok(Self { transaction }),
            other => anyhow::bail!("Wrong transaction type for expense: {other:?}"),
        }
    }
}

#[cfg(any(test, feature = "test-helpers"))]
impl Expense {
    /// Creates a random value for testing
    pub fn sample() -> Self {
        let transaction = Transaction::sample_with_type(TransactionType::Expense);
        Expense { transaction }
    }

    /// Creates a random value for testing with a specific identifier
    pub fn sample_with_id(id: TransactionId) -> Self {
        let transaction =
            Transaction::sample_parameterized(Some(id), Some(TransactionType::Expense));
        Expense { transaction }
    }

    /// Creates a random value for testing with a specific identifier
    pub fn sample_with_owner(owner: TransactionActor) -> Self {
        let transaction = Transaction::sample_with_owner(owner);
        Expense { transaction }
    }
}

#[cfg(test)]
mod tests {
    use crate::test_helpers;

    use super::*;

    #[test]
    fn should_create_new() {
        let (id, ts, tg, am, de) = (
            TransactionId::sample(),
            Utc::now(),
            TransactionActor::sample(),
            MonetaryValue::sample(),
            test_helpers::random_string(16),
        );

        let expense = Expense::new(id, ts, tg, am, Some(de.clone()));

        assert_eq!(expense.transaction.id(), id);
        assert_eq!(expense.transaction.timestamp(), ts);
        assert_eq!(expense.transaction.owner(), tg);
        assert_eq!(expense.transaction.amount(), am);
        assert_eq!(expense.transaction.description(), Some(&de));
        assert_eq!(
            expense.transaction.transaction_type(),
            TransactionType::Expense
        );
    }

    #[test]
    fn should_return_getters() {
        let expense = Expense::sample();

        assert_eq!(expense.transaction.id(), expense.id());
        assert_eq!(expense.transaction.timestamp(), expense.timestamp());
        assert_eq!(expense.transaction.owner(), expense.owner());
        assert_eq!(expense.transaction.amount(), expense.amount());
        assert_eq!(expense.transaction.description(), expense.description());
        assert_eq!(*expense.transaction(), expense.transaction);
    }

    #[test]
    fn should_parse_from_transaction() {
        let transaction = Transaction::sample_with_type(TransactionType::Expense);

        let expense =
            Expense::try_from(transaction.clone()).expect("Could not parse from transaction");

        assert_eq!(expense.transaction, transaction);
    }

    #[test]
    fn parse_from_transaction_should_fail_if_wrong_type() {
        let transactions = [
            Transaction::sample_with_type(TransactionType::Earning),
            Transaction::sample_with_type(
                TransactionType::Transference(TransactionActor::sample()),
            ),
        ];

        for transaction in transactions.into_iter() {
            let res = Expense::try_from(transaction.clone());
            assert!(res.is_err());
        }
    }
}
