use chrono::{DateTime, Utc};

use crate::{MonetaryValue, Transaction, TransactionActor, TransactionId};

use super::transaction::TransactionType;

#[derive(Debug, Clone, PartialEq, Eq)]
/// Represents a transference in the system
pub struct Transference {
    /// The internal transaction of the earning
    transaction: Transaction,
    /// Shortcut for the transaction target
    target: TransactionActor,
}

impl Transference {
    /// Creates a new [Transference]
    pub fn new(
        id: TransactionId,
        timestamp: DateTime<Utc>,
        source: TransactionActor,
        target: TransactionActor,
        amount: MonetaryValue,
        description: Option<String>,
    ) -> Self {
        let transaction = Transaction::new(
            id,
            timestamp,
            source,
            amount,
            description,
            TransactionType::Transference(target),
        );
        Transference {
            transaction,
            target,
        }
    }

    /// Source getter
    pub fn source(&self) -> TransactionActor {
        self.transaction.owner()
    }

    /// Target getter
    pub fn target(&self) -> TransactionActor {
        self.target
    }

    /// Internal transaction getter
    pub fn transaction(&self) -> &Transaction {
        &self.transaction
    }

    // Delegates all methods of the internal transaction
    delegate::delegate! {
        to self.transaction {
            /// Identifier getter
            pub fn id(&self) -> TransactionId;
            /// Timestamp getter
            pub fn timestamp(&self) -> DateTime<Utc>;
            /// Amount getter
            pub fn amount(&self) -> MonetaryValue;
            /// Description getter
            pub fn description(&self) -> Option<&String>;
        }
    }
}

#[cfg(any(test, feature = "test-helpers"))]
impl Transference {
    /// Creates a random value for testing
    pub fn sample() -> Self {
        let target = TransactionActor::sample();
        let transaction = Transaction::sample_with_type(TransactionType::Transference(target));
        Transference {
            transaction,
            target,
        }
    }

    /// Creates a random value for testing with a specific identifier
    pub fn sample_with_id(id: TransactionId) -> Self {
        let target = TransactionActor::sample();
        let transaction = Transaction::sample_parameterized(
            Some(id),
            Some(TransactionType::Transference(target)),
        );
        Transference {
            transaction,
            target,
        }
    }
}

impl TryFrom<Transaction> for Transference {
    type Error = anyhow::Error;
    /// Creates a new [Transaction] with the inner transference
    ///
    /// # Errors
    ///
    /// - The transaction is not from the correct type
    fn try_from(transaction: Transaction) -> Result<Self, Self::Error> {
        match transaction.transaction_type() {
            TransactionType::Transference(target) => Ok(Self {
                transaction,
                target,
            }),
            other => anyhow::bail!("Wrong transaction type for transference: {other:?}"),
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::test_helpers;

    use super::*;

    #[test]
    fn should_create_new() {
        let (id, ts, sr, tg, am, de) = (
            TransactionId::sample(),
            Utc::now(),
            TransactionActor::sample(),
            TransactionActor::sample(),
            MonetaryValue::sample(),
            test_helpers::random_string(16),
        );

        let transference = Transference::new(id, ts, sr, tg, am, Some(de.clone()));

        assert_eq!(transference.transaction.id(), id);
        assert_eq!(transference.transaction.timestamp(), ts);
        assert_eq!(transference.transaction.owner(), sr);
        assert_eq!(transference.transaction.amount(), am);
        assert_eq!(transference.transaction.description(), Some(&de));
        assert_eq!(
            transference.transaction.transaction_type(),
            TransactionType::Transference(tg)
        );
        assert_eq!(transference.target, tg);
    }

    #[test]
    fn should_return_getters() {
        let transference = Transference::sample();

        assert_eq!(transference.transaction.id(), transference.id());
        assert_eq!(
            transference.transaction.timestamp(),
            transference.timestamp()
        );
        assert_eq!(transference.transaction.amount(), transference.amount());
        assert_eq!(
            transference.transaction.description(),
            transference.description()
        );
        assert_eq!(transference.source(), transference.transaction.owner());
        assert_eq!(transference.target(), transference.target);
        assert_eq!(*transference.transaction(), transference.transaction);
    }

    #[test]
    fn should_parse_from_transaction() {
        let target = TransactionActor::sample();
        let transaction = Transaction::sample_with_type(TransactionType::Transference(target));

        let transference =
            Transference::try_from(transaction.clone()).expect("Could not parse from transaction");

        assert_eq!(transference.transaction, transaction);
        assert_eq!(transference.target, target);
    }

    #[test]
    fn parse_from_transaction_should_fail_if_wrong_type() {
        let transactions = [
            Transaction::sample_with_type(TransactionType::Earning),
            Transaction::sample_with_type(TransactionType::Expense),
        ];

        for transaction in transactions.into_iter() {
            let res = Transference::try_from(transaction.clone());
            assert!(res.is_err());
        }
    }
}
