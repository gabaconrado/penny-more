use chrono::{DateTime, Utc};

use crate::{AccountId, CardId, MonetaryValue, TransactionId};

/// Object to represent a monetary transaction in the system
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Transaction {
    /// The transaction identifier
    id: TransactionId,
    /// The timestamp of the transaction
    timestamp: DateTime<Utc>,
    /// The account/card that the initiates the operation
    owner: TransactionActor,
    /// The transaction amount
    amount: MonetaryValue,
    /// Optional description of the transaction
    description: Option<String>,
    /// The type of the transaction
    transaction_type: TransactionType,
}

impl Transaction {
    /// Creates a new [Transaction]
    pub(crate) fn new(
        id: TransactionId,
        timestamp: DateTime<Utc>,
        owner: TransactionActor,
        amount: MonetaryValue,
        description: Option<String>,
        transaction_type: TransactionType,
    ) -> Self {
        Self {
            id,
            timestamp,
            owner,
            amount,
            description,
            transaction_type,
        }
    }

    /// Identifier getter
    pub fn id(&self) -> TransactionId {
        self.id
    }

    /// Timestamp getter
    pub fn timestamp(&self) -> DateTime<Utc> {
        self.timestamp
    }

    /// Transaction owner getter
    pub fn owner(&self) -> TransactionActor {
        self.owner
    }

    /// Amount getter
    pub fn amount(&self) -> MonetaryValue {
        self.amount
    }

    /// Description getter
    pub fn description(&self) -> Option<&String> {
        self.description.as_ref()
    }

    /// Transaction type getter
    pub fn transaction_type(&self) -> TransactionType {
        self.transaction_type
    }
}

/// Represents all the possible member for a monetary transaction
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum TransactionActor {
    /// Transaction member is an account, holds the account id
    Account(AccountId),
    /// Transaction member is a card, holds the card id
    Card(CardId),
}

/// Represents all the possible targets of a monetary transaction
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum TransactionType {
    /// Money was received
    Earning,
    /// Money was charged
    Expense,
    /// Money was transferred from an account to another account or card, holds
    /// the receiving member of the transaction
    Transference(TransactionActor),
}

#[cfg(any(test, feature = "test-helpers"))]
impl Transaction {
    /// Creates a random value for testing
    pub fn sample() -> Self {
        let id = TransactionId::sample();
        let timestamp = Utc::now();
        let owner = TransactionActor::sample();
        let amount = MonetaryValue::sample();
        let description = Some(crate::test_helpers::random_string(32));
        let transaction_type = TransactionType::sample();
        Transaction::new(id, timestamp, owner, amount, description, transaction_type)
    }

    /// Creates a random value for testing
    pub fn sample_with_id(id: TransactionId) -> Self {
        let timestamp = Utc::now();
        let owner = TransactionActor::sample();
        let amount = MonetaryValue::sample();
        let description = Some(crate::test_helpers::random_string(32));
        let transaction_type = TransactionType::sample();
        Transaction::new(id, timestamp, owner, amount, description, transaction_type)
    }

    /// Creates a random value for testing
    pub fn sample_with_type(transaction_type: TransactionType) -> Self {
        let mut transaction = Transaction::sample();
        transaction.transaction_type = transaction_type;
        transaction
    }

    /// Creates a random value for testing
    pub fn sample_with_owner(owner: TransactionActor) -> Self {
        let mut transaction = Transaction::sample();
        transaction.owner = owner;
        transaction
    }

    /// Creates a random value for testing
    pub fn sample_parameterized(
        id: Option<TransactionId>,
        transaction_type: Option<TransactionType>,
    ) -> Self {
        let mut transaction = Transaction::sample();
        transaction.id = id.unwrap_or_else(TransactionId::sample);
        transaction.transaction_type = transaction_type.unwrap_or_else(TransactionType::sample);
        transaction
    }
}

#[cfg(any(test, feature = "test-helpers"))]
impl TransactionActor {
    /// Creates a random value for testing
    pub fn sample() -> Self {
        use rand::seq::SliceRandom;
        let et = [
            TransactionActor::Account(AccountId::sample()),
            TransactionActor::Card(CardId::sample()),
        ];
        let et = et
            .choose(&mut rand::thread_rng())
            .expect("Unexpected empty array");
        *et
    }
}

#[cfg(any(test, feature = "test-helpers"))]
impl TransactionType {
    /// Creates a random value for testing
    pub fn sample() -> Self {
        use rand::seq::SliceRandom;
        let et = [
            TransactionType::Earning,
            TransactionType::Expense,
            TransactionType::Transference(TransactionActor::sample()),
        ];
        let et = et
            .choose(&mut rand::thread_rng())
            .expect("Unexpected empty array");
        *et
    }
}

#[cfg(test)]
mod tests {

    mod new {
        use super::super::*;

        #[test]
        fn should_create_new() {
            let id = TransactionId::sample();
            let timestamp = Utc::now();
            let owner = TransactionActor::sample();
            let amount = MonetaryValue::sample();
            let description = Some(crate::test_helpers::random_string(32));
            let transaction_type = TransactionType::sample();

            let transaction = Transaction::new(
                id,
                timestamp,
                owner,
                amount,
                description.clone(),
                transaction_type,
            );

            assert_eq!(transaction.id, id);
            assert_eq!(transaction.timestamp, timestamp);
            assert_eq!(transaction.owner, owner);
            assert_eq!(transaction.amount, amount);
            assert_eq!(transaction.description, description);
            assert_eq!(transaction.transaction_type, transaction_type);
        }

        #[test]
        fn should_return_getters() {
            let transaction = Transaction::sample();

            assert_eq!(transaction.id, transaction.id());
            assert_eq!(transaction.timestamp, transaction.timestamp());
            assert_eq!(transaction.owner, transaction.owner());
            assert_eq!(transaction.amount, transaction.amount());
            assert_eq!(transaction.description.as_ref(), transaction.description());
            assert_eq!(transaction.transaction_type, transaction.transaction_type());
        }
    }
}
