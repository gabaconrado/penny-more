use chrono::{DateTime, Utc};

use crate::{MonetaryValue, Transaction, TransactionActor, TransactionId};

use super::transaction::TransactionType;

#[derive(Debug, Clone, PartialEq, Eq)]
/// Represents an earning in the system
pub struct Earning {
    /// The internal transaction of the earning
    transaction: Transaction,
}

impl Earning {
    /// Creates a new [Earning]
    pub fn new(
        id: TransactionId,
        timestamp: DateTime<Utc>,
        owner: TransactionActor,
        amount: MonetaryValue,
        description: Option<String>,
    ) -> Self {
        let transaction = Transaction::new(
            id,
            timestamp,
            owner,
            amount,
            description,
            TransactionType::Earning,
        );
        Earning { transaction }
    }

    /// Internal transaction getter
    pub fn transaction(&self) -> &Transaction {
        &self.transaction
    }

    // Delegates all methods of the internal transaction
    delegate::delegate! {
        to self.transaction {
            /// Identifier getter
            pub fn id(&self) -> TransactionId;
            /// Timestamp getter
            pub fn timestamp(&self) -> DateTime<Utc>;
            /// Transaction owner getter
            pub fn owner(&self) -> TransactionActor;
            /// Amount getter
            pub fn amount(&self) -> MonetaryValue;
            /// Description getter
            pub fn description(&self) -> Option<&String>;
        }
    }
}

impl TryFrom<Transaction> for Earning {
    type Error = anyhow::Error;
    /// Creates a new [Earning] with the inner transference
    ///
    /// # Errors
    ///
    /// - The transaction is not from the correct type
    fn try_from(transaction: Transaction) -> Result<Self, Self::Error> {
        match transaction.transaction_type() {
            TransactionType::Earning => Ok(Self { transaction }),
            other => anyhow::bail!("Wrong transaction type for earning: {other:?}"),
        }
    }
}

#[cfg(any(test, feature = "test-helpers"))]
impl Earning {
    /// Creates a random value for testing
    pub fn sample() -> Self {
        let transaction = Transaction::sample_with_type(TransactionType::Earning);
        Earning { transaction }
    }

    /// Creates a random value for testing with a specific identifier
    pub fn sample_with_id(id: TransactionId) -> Self {
        let transaction =
            Transaction::sample_parameterized(Some(id), Some(TransactionType::Earning));
        Earning { transaction }
    }
}

#[cfg(test)]
mod tests {
    use crate::test_helpers;

    use super::*;

    #[test]
    fn should_create_new() {
        let (id, ts, tg, am, de) = (
            TransactionId::sample(),
            Utc::now(),
            TransactionActor::sample(),
            MonetaryValue::sample(),
            test_helpers::random_string(16),
        );

        let earning = Earning::new(id, ts, tg, am, Some(de.clone()));

        assert_eq!(earning.transaction.id(), id);
        assert_eq!(earning.transaction.timestamp(), ts);
        assert_eq!(earning.transaction.owner(), tg);
        assert_eq!(earning.transaction.amount(), am);
        assert_eq!(earning.transaction.description(), Some(&de));
        assert_eq!(
            earning.transaction.transaction_type(),
            TransactionType::Earning
        );
    }

    #[test]
    fn should_return_getters() {
        let earning = Earning::sample();

        assert_eq!(earning.transaction.id(), earning.id());
        assert_eq!(earning.transaction.timestamp(), earning.timestamp());
        assert_eq!(earning.transaction.owner(), earning.owner());
        assert_eq!(earning.transaction.amount(), earning.amount());
        assert_eq!(earning.transaction.description(), earning.description());
        assert_eq!(*earning.transaction(), earning.transaction);
    }

    #[test]
    fn should_parse_from_transaction() {
        let transaction = Transaction::sample_with_type(TransactionType::Earning);

        let earning =
            Earning::try_from(transaction.clone()).expect("Could not parse from transaction");

        assert_eq!(earning.transaction, transaction);
    }

    #[test]
    fn parse_from_transaction_should_fail_if_wrong_type() {
        let transactions = [
            Transaction::sample_with_type(TransactionType::Expense),
            Transaction::sample_with_type(
                TransactionType::Transference(TransactionActor::sample()),
            ),
        ];

        for transaction in transactions.into_iter() {
            let res = Earning::try_from(transaction.clone());
            assert!(res.is_err());
        }
    }
}
