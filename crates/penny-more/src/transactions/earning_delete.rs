use crate::{DeleteTransaction, TransactionId};

/// Defines the behavior of an object that can delete an Earning
pub trait DeleteEarning: DeleteTransaction {
    /// Deletes an earning from the system
    ///
    /// Default implementation calls the [DeleteTransaction] implementation
    fn delete(&self, earning: TransactionId) -> anyhow::Result<()> {
        DeleteTransaction::delete(self, earning)
    }
}

/// Manages the deletion of an Earning
///
/// Simply calls the underlying data storage to remove the earning
///
/// # Errors
///
/// - Internal;
pub fn delete_earning_controller<D>(
    delete_earning: &D,
    earning: TransactionId,
) -> anyhow::Result<()>
where
    D: DeleteEarning,
{
    DeleteEarning::delete(delete_earning, earning)?;
    Ok(())
}

#[cfg(any(test, feature = "test-helpers"))]
pub mod mock {
    use super::*;

    /// Mock [DeleteEarning] object useful for testing
    #[derive(Clone, Debug, Default)]
    pub struct MockDeleteEarning {
        /// Indicates function should return an error
        pub err: bool,
    }

    impl DeleteTransaction for MockDeleteEarning {
        /// Default successful response
        ///
        /// # Error
        ///
        /// - Internal flag set;
        fn delete(&self, _transaction: TransactionId) -> anyhow::Result<()> {
            if self.err {
                anyhow::bail!("MockDeleteEarning internal flag set")
            }
            Ok(())
        }
    }

    impl DeleteEarning for MockDeleteEarning {}
}

#[cfg(test)]
mod tests {

    mod controller {
        use crate::MockDeleteEarning;

        use super::super::*;

        #[test]
        fn should_delete_transaction() -> anyhow::Result<()> {
            let dt = MockDeleteEarning::default();
            let earning = TransactionId::sample();

            delete_earning_controller(&dt, earning)?;

            Ok(())
        }

        #[test]
        fn should_error_if_internal_flag_set() {
            let dt = MockDeleteEarning { err: true };
            let earning = TransactionId::sample();

            let res = delete_earning_controller(&dt, earning);

            assert!(res.is_err());
        }
    }
}
