use crate::{DeleteTransaction, TransactionId};

/// Defines the behavior of an object that can delete an Transference
pub trait DeleteTransference: DeleteTransaction {
    /// Deletes an transference from the system
    ///
    /// Default implementation calls the [DeleteTransaction] implementation
    fn delete(&self, transference: TransactionId) -> anyhow::Result<()> {
        DeleteTransaction::delete(self, transference)
    }
}

/// Manages the deletion of a Transference
///
/// Simply calls the underlying data storage to remove the transference
///
/// # Errors
///
/// - Internal;
pub fn delete_transference_controller<D>(
    delete_transference: &D,
    transference: TransactionId,
) -> anyhow::Result<()>
where
    D: DeleteTransference,
{
    DeleteTransference::delete(delete_transference, transference)?;
    Ok(())
}

#[cfg(any(test, feature = "test-helpers"))]
pub mod mock {
    use super::*;

    /// Mock [DeleteTransference] object useful for testing
    #[derive(Clone, Debug, Default)]
    pub struct MockDeleteTransference {
        /// Indicates function should return an error
        pub err: bool,
    }

    impl DeleteTransaction for MockDeleteTransference {
        /// Default successful response
        ///
        /// # Error
        ///
        /// - Internal flag set;
        fn delete(&self, _transaction: TransactionId) -> anyhow::Result<()> {
            if self.err {
                anyhow::bail!("MockDeleteTransference internal flag set")
            }
            Ok(())
        }
    }

    impl DeleteTransference for MockDeleteTransference {}
}

#[cfg(test)]
mod tests {

    mod controller {
        use crate::MockDeleteTransference;

        use super::super::*;

        #[test]
        fn should_delete_transaction() -> anyhow::Result<()> {
            let dt = MockDeleteTransference::default();
            let transference = TransactionId::sample();

            delete_transference_controller(&dt, transference)?;

            Ok(())
        }

        #[test]
        fn should_error_if_internal_flag_set() {
            let dt = MockDeleteTransference { err: true };
            let transference = TransactionId::sample();

            let res = delete_transference_controller(&dt, transference);

            assert!(res.is_err());
        }
    }
}
