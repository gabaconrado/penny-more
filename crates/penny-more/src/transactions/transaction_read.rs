use crate::{Transaction, TransactionId};

/// Defines the behavior of an object that can read stored Transactions
pub trait ReadTransaction {
    /// Reads all saved transactions
    fn read_all(&self) -> anyhow::Result<Vec<Transaction>>;

    /// Reads a transaction by its ID
    ///
    /// # Errors
    ///
    /// - Transaction does not exist;
    fn read(&self, id: TransactionId) -> anyhow::Result<Transaction>;
}

/// Manages the listing of all transactions
pub fn list_transactions_controller<R>(read_transaction: &R) -> anyhow::Result<Vec<Transaction>>
where
    R: ReadTransaction,
{
    let transactions = read_transaction.read_all()?;
    Ok(transactions)
}

#[cfg(any(test, feature = "test-helpers"))]
pub mod mock {

    use chrono::Utc;

    use crate::{
        test_helpers, transactions::types::transaction::TransactionType, MonetaryValue,
        TransactionActor,
    };

    use super::*;

    /// Mock [ReadTransaction] object useful for tests
    #[derive(Debug, Clone, Default)]
    pub struct MockReadTransaction {
        /// Indicates if the functions should return errors
        pub err: bool,
        /// The array of transactions to be returned by the methods
        pub transactions: Vec<Transaction>,
    }

    impl ReadTransaction for MockReadTransaction {
        /// Returns the internal vector of cards
        ///
        /// # Errors
        ///
        /// - Internal flag set;
        fn read_all(&self) -> anyhow::Result<Vec<Transaction>> {
            if self.err {
                anyhow::bail!("MockReadTransaction internal error flag set");
            }
            Ok(self.transactions.clone())
        }

        /// Searches the internal vector for the requested transaction and returns it
        ///
        /// Returns a random [Transaction] object with the requested id if it is not
        /// in the internal vector
        ///
        /// # Errors
        ///
        /// - Internal flag set;
        fn read(&self, id: TransactionId) -> anyhow::Result<Transaction> {
            if self.err {
                anyhow::bail!("MockReadTransaction internal error flag set");
            }
            let transaction = self
                .transactions
                .iter()
                .cloned()
                .find(|a| a.id() == id)
                .unwrap_or_else(|| {
                    Transaction::new(
                        id,
                        Utc::now(),
                        TransactionActor::sample(),
                        MonetaryValue::sample(),
                        Some(test_helpers::random_string(32)),
                        TransactionType::sample(),
                    )
                });
            Ok(transaction)
        }
    }
}

#[cfg(test)]
mod tests {
    mod controller {
        use crate::MockReadTransaction;

        use super::super::*;

        #[test]
        fn should_list_transactions() -> anyhow::Result<()> {
            let expected = vec![Transaction::sample(), Transaction::sample()];
            let rt = MockReadTransaction {
                transactions: expected.clone(),
                ..Default::default()
            };

            let transactions = list_transactions_controller(&rt)?;

            assert_eq!(expected, transactions);
            Ok(())
        }

        #[test]
        fn should_error_if_internal_flag_set() {
            let rt = MockReadTransaction {
                err: true,
                ..Default::default()
            };

            let res = list_transactions_controller(&rt);

            assert!(res.is_err());
        }
    }
}
