use chrono::{DateTime, Utc};

use crate::{
    MonetaryValue, ReadTransference, Transaction, TransactionActor, TransactionId, Transference,
    UpdateTransaction,
};

/// Defines the behavior of an object that can update a Transaction
pub trait UpdateTransference: UpdateTransaction {
    /// Updates a transaction in the system
    ///
    /// # Errors
    ///
    /// - Internal;
    fn update(&self, transference: &Transference) -> anyhow::Result<()> {
        UpdateTransaction::update(self, transference.transaction())
    }
}

/// Object that can be used to translate what should be updated in a [Transference]
#[derive(Debug, Clone)]
pub struct TransferenceChangeSet {
    /// Transference that will be changed
    pub id: TransactionId,
    /// New timestamp, [None] means to not update value
    pub timestamp: Option<DateTime<Utc>>,
    /// New source, [None] means to not update value
    pub source: Option<TransactionActor>,
    /// New target, [None] means to not update value
    pub target: Option<TransactionActor>,
    /// New amount, [None] means to not update value
    pub amount: Option<MonetaryValue>,
    /// New description, [None] means to not update value
    pub description: Option<Option<String>>,
}

impl TransferenceChangeSet {
    /// Creates a new [TransferenceChangeSet]
    pub fn new(
        id: TransactionId,
        timestamp: Option<DateTime<Utc>>,
        source: Option<TransactionActor>,
        target: Option<TransactionActor>,
        amount: Option<MonetaryValue>,
        description: Option<Option<String>>,
    ) -> Self {
        Self {
            id,
            timestamp,
            source,
            target,
            amount,
            description,
        }
    }
}

/// Manages the updating of an transference
///
/// # Errors
///
/// - Transference does not exist;
/// - Internal;
pub fn update_transference_controller<R, U>(
    read_transference: &R,
    upd_transference: &U,
    transference_cs: TransferenceChangeSet,
) -> anyhow::Result<()>
where
    U: UpdateTransference,
    R: ReadTransference,
{
    let saved_transference = ReadTransference::read(read_transference, transference_cs.id)?;

    let id = transference_cs.id;
    let timestamp = transference_cs
        .timestamp
        .unwrap_or_else(|| saved_transference.timestamp());
    let source = transference_cs
        .source
        .unwrap_or_else(|| saved_transference.source());
    let target = transference_cs
        .target
        .unwrap_or_else(|| saved_transference.target());
    let amount = transference_cs
        .amount
        .unwrap_or_else(|| saved_transference.amount());
    let description = transference_cs
        .description
        .unwrap_or_else(|| saved_transference.description().cloned());
    let transference = Transference::new(id, timestamp, source, target, amount, description);

    UpdateTransference::update(upd_transference, &transference)?;

    Ok(())
}

#[cfg(any(test, feature = "test-helpers"))]
pub mod mock {
    use super::*;

    /// Mock [UpdateTransference] object useful for testing
    #[derive(Debug, Default)]
    pub struct MockUpdateTransference {
        /// Holds a clone of the last transference object that was used in an update call
        pub expected_transference: Option<Transference>,
        /// Indicates function should return an error
        pub err: bool,
    }

    impl MockUpdateTransference {
        /// Sets an expected transaction object to be matched in the update method call
        ///
        /// Replaces any existing transaction that was added before
        pub fn set_expected_transference(&mut self, transference: &Transference) {
            self.expected_transference = Some(transference.clone());
        }
    }

    impl UpdateTransaction for MockUpdateTransference {
        /// Default successful response
        ///
        /// # Error
        ///
        /// - Internal error flag set;
        fn update(&self, transaction: &Transaction) -> anyhow::Result<()> {
            if self.err {
                anyhow::bail!("MockUpdateTransference internal error flag set");
            }
            if let Some(t) = &self.expected_transference {
                if t.transaction() != transaction {
                    anyhow::bail!(
                        "Passed transference is different than expected: \n{transaction:#?} \n{t:#?}"
                    );
                }
            }
            Ok(())
        }
    }

    impl UpdateTransference for MockUpdateTransference {}
}

#[cfg(test)]
mod tests {

    mod transference_change_set {
        use crate::test_helpers;

        use super::super::*;

        #[test]
        fn should_create_new() {
            let id = TransactionId::sample();
            let timestamp = Some(Utc::now());
            let source = Some(TransactionActor::sample());
            let target = Some(TransactionActor::sample());
            let amount = Some(MonetaryValue::sample());
            let description = Some(Some(test_helpers::random_string(16)));

            let tcs = TransferenceChangeSet::new(
                id,
                timestamp,
                source,
                target,
                amount,
                description.clone(),
            );

            assert_eq!(tcs.id, id);
            assert_eq!(tcs.timestamp, timestamp);
            assert_eq!(tcs.source, source);
            assert_eq!(tcs.target, target);
            assert_eq!(tcs.amount, amount);
            assert_eq!(tcs.description, description);
        }
    }

    mod controller {
        use crate::{test_helpers, MockReadTransference, MockUpdateTransference};

        use super::super::*;

        #[test]
        fn should_update_transference() -> anyhow::Result<()> {
            let transference = Transference::sample();
            let rt = MockReadTransference {
                transferences: vec![transference.clone()],
                ..Default::default()
            };
            let mut ut = MockUpdateTransference::default();

            // Updating timestamp
            let new_timestamp = Utc::now();
            let transference_cs = TransferenceChangeSet::new(
                transference.id(),
                Some(new_timestamp),
                None,
                None,
                None,
                None,
            );
            let expected_transference = Transference::new(
                transference.id(),
                new_timestamp,
                transference.source(),
                transference.target(),
                transference.amount(),
                transference.description().cloned(),
            );
            ut.set_expected_transference(&expected_transference);
            update_transference_controller(&rt, &ut, transference_cs)?;
            println!("Here");

            // Updating source
            let new_source = TransactionActor::sample();
            let transference_cs = TransferenceChangeSet::new(
                transference.id(),
                None,
                Some(new_source),
                None,
                None,
                None,
            );
            let expected_transference = Transference::new(
                transference.id(),
                transference.timestamp(),
                new_source,
                transference.target(),
                transference.amount(),
                transference.description().cloned(),
            );
            ut.set_expected_transference(&expected_transference);
            update_transference_controller(&rt, &ut, transference_cs)?;
            println!("Here2");

            // Updating target
            let new_target = TransactionActor::sample();
            let transference_cs = TransferenceChangeSet::new(
                transference.id(),
                None,
                None,
                Some(new_target),
                None,
                None,
            );
            let expected_transference = Transference::new(
                transference.id(),
                transference.timestamp(),
                transference.source(),
                new_target,
                transference.amount(),
                transference.description().cloned(),
            );
            ut.set_expected_transference(&expected_transference);
            update_transference_controller(&rt, &ut, transference_cs)?;
            println!("Here3");

            // Updating amount
            let new_amount = MonetaryValue::sample();
            let transference_cs = TransferenceChangeSet::new(
                transference.id(),
                None,
                None,
                None,
                Some(new_amount),
                None,
            );
            let expected_transference = Transference::new(
                transference.id(),
                transference.timestamp(),
                transference.source(),
                transference.target(),
                new_amount,
                transference.description().cloned(),
            );
            ut.set_expected_transference(&expected_transference);
            update_transference_controller(&rt, &ut, transference_cs)?;
            println!("Here4");

            // Updating description
            let new_description = Some(test_helpers::random_string(16));
            let transference_cs = TransferenceChangeSet::new(
                transference.id(),
                None,
                None,
                None,
                None,
                Some(new_description.clone()),
            );
            let expected_transference = Transference::new(
                transference.id(),
                transference.timestamp(),
                transference.source(),
                transference.target(),
                transference.amount(),
                new_description,
            );
            ut.set_expected_transference(&expected_transference);
            update_transference_controller(&rt, &ut, transference_cs)?;
            println!("Here5");

            // Updating nothing
            let transference_cs =
                TransferenceChangeSet::new(transference.id(), None, None, None, None, None);
            let expected_transference = transference;
            ut.set_expected_transference(&expected_transference);
            update_transference_controller(&rt, &ut, transference_cs)?;

            Ok(())
        }

        #[test]
        fn should_error_if_update_fails() {
            let rt = MockReadTransference::default();
            let ut = MockUpdateTransference {
                err: true,
                ..Default::default()
            };
            let transference = Transaction::sample();
            let transference_cs =
                TransferenceChangeSet::new(transference.id(), None, None, None, None, None);

            let res = update_transference_controller(&rt, &ut, transference_cs);

            assert!(res.is_err());
        }

        #[test]
        fn should_error_if_read_fails() {
            let rt = MockReadTransference {
                err: true,
                ..Default::default()
            };
            let ut = MockUpdateTransference::default();
            let transference = Transference::sample();
            let transference_cs =
                TransferenceChangeSet::new(transference.id(), None, None, None, None, None);

            let res = update_transference_controller(&rt, &ut, transference_cs);

            assert!(res.is_err());
        }
    }
}
