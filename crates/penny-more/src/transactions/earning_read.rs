use crate::{Earning, ReadTransaction, TransactionId};

/// Defines the behavior of an object that can read stored Earnings
pub trait ReadEarning: ReadTransaction {
    /// Reads all saved earnings
    ///
    /// # Errors
    ///
    /// - Error parsing any transaction;
    /// - Internal;
    fn read_all(&self) -> anyhow::Result<Vec<Earning>> {
        ReadTransaction::read_all(self)?
            .into_iter()
            .map(Earning::try_from)
            .collect()
    }

    /// Reads an earning by its ID
    ///
    /// Calls the internal read transaction
    ///
    /// # Errors
    ///
    /// - Earning does not exist;
    /// - Error parsing transaction;
    fn read(&self, id: TransactionId) -> anyhow::Result<Earning> {
        Earning::try_from(ReadTransaction::read(self, id)?)
    }
}

/// Manages the listing of all earnings
pub fn list_earnings_controller<R>(read_earning: &R) -> anyhow::Result<Vec<Earning>>
where
    R: ReadEarning,
{
    let earnings = ReadEarning::read_all(read_earning)?;
    Ok(earnings)
}

#[cfg(any(test, feature = "test-helpers"))]
pub mod mock {

    use chrono::Utc;

    use crate::{test_helpers, MonetaryValue, Transaction, TransactionActor};

    use super::*;

    /// Mock [ReadEarning] object useful for tests
    #[derive(Debug, Clone, Default)]
    pub struct MockReadEarning {
        /// Indicates if the functions should return errors
        pub err: bool,
        /// The array of earnings to be returned by the methods
        pub earnings: Vec<Earning>,
    }

    impl ReadTransaction for MockReadEarning {
        /// Returns the internal vector of cards
        ///
        /// # Errors
        ///
        /// - Internal flag set;
        fn read_all(&self) -> anyhow::Result<Vec<Transaction>> {
            if self.err {
                anyhow::bail!("MockReadEarning internal error flag set");
            }
            Ok(self
                .earnings
                .iter()
                .map(|e| e.transaction())
                .cloned()
                .collect())
        }

        /// Searches the internal vector for the requested earning and returns it
        ///
        /// Returns a random [Earning] object with the requested id if it is not
        /// in the internal vector
        ///
        /// # Errors
        ///
        /// - Internal flag set;
        fn read(&self, id: TransactionId) -> anyhow::Result<Transaction> {
            if self.err {
                anyhow::bail!("MockReadEarning internal error flag set");
            }
            let earning = self
                .earnings
                .iter()
                .cloned()
                .find(|a| a.id() == id)
                .unwrap_or_else(|| {
                    Earning::new(
                        id,
                        Utc::now(),
                        TransactionActor::sample(),
                        MonetaryValue::sample(),
                        Some(test_helpers::random_string(32)),
                    )
                });
            Ok(earning.transaction().clone())
        }
    }

    impl ReadEarning for MockReadEarning {}
}

#[cfg(test)]
mod tests {
    mod controller {
        use crate::MockReadEarning;

        use super::super::*;

        #[test]
        fn should_list_earnings() -> anyhow::Result<()> {
            let expected = vec![Earning::sample(), Earning::sample()];
            let rt = MockReadEarning {
                earnings: expected.clone(),
                ..Default::default()
            };

            let earnings = list_earnings_controller(&rt)?;

            assert_eq!(expected, earnings);
            Ok(())
        }

        #[test]
        fn should_error_if_internal_flag_set() {
            let rt = MockReadEarning {
                err: true,
                ..Default::default()
            };

            let res = list_earnings_controller(&rt);

            assert!(res.is_err());
        }
    }
}
