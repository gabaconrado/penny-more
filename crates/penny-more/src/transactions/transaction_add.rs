use crate::Transaction;

/// Defines the behavior of an object that can add an Transaction
pub trait AddTransaction {
    /// Adds a new transaction into the system
    fn add(&self, transaction: &Transaction) -> anyhow::Result<()>;
}

/// Manages the adding of a new transaction
///
/// Simply calls the underlying data storage to add the transaction
///
/// # Errors
///
/// - Internal;
pub fn add_transaction_controller<A>(
    add_transaction: &A,
    transaction: &Transaction,
) -> anyhow::Result<()>
where
    A: AddTransaction,
{
    add_transaction.add(transaction)?;
    Ok(())
}

#[cfg(any(test, feature = "test-helpers"))]
pub mod mock {
    use super::*;

    /// Mock [AddTransaction] object useful for testing
    #[derive(Clone, Debug, Default)]
    pub struct MockAddTransaction {
        /// Indicates function should return an error
        pub err: bool,
    }

    impl AddTransaction for MockAddTransaction {
        /// Default successful response
        ///
        /// # Error
        ///
        /// - Internal flag set;
        fn add(&self, _transaction: &Transaction) -> anyhow::Result<()> {
            if self.err {
                anyhow::bail!("MockAddTransaction internal flag set")
            }
            Ok(())
        }
    }
}

#[cfg(test)]
mod tests {

    mod controller {
        use crate::MockAddTransaction;

        use super::super::*;

        #[test]
        fn should_add_transaction() -> anyhow::Result<()> {
            let at = MockAddTransaction::default();
            let transaction = Transaction::sample();

            add_transaction_controller(&at, &transaction)?;

            Ok(())
        }

        #[test]
        fn should_error_if_internal_flag_set() {
            let transaction = Transaction::sample();
            let at = MockAddTransaction { err: true };

            let res = add_transaction_controller(&at, &transaction);

            assert!(res.is_err());
        }
    }
}
