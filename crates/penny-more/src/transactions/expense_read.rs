use crate::{Expense, ReadTransaction, TransactionId};

/// Defines the behavior of an object that can read stored Expenses
pub trait ReadExpense: ReadTransaction {
    /// Reads all saved expenses
    ///
    /// # Errors
    ///
    /// - Error parsing any transaction;
    /// - Internal;
    fn read_all(&self) -> anyhow::Result<Vec<Expense>> {
        ReadTransaction::read_all(self)?
            .into_iter()
            .map(Expense::try_from)
            .collect()
    }

    /// Reads an Expense by its ID
    ///
    /// Calls the internal read transaction
    ///
    /// # Errors
    ///
    /// - Expense does not exist;
    /// - Error parsing transaction;
    fn read(&self, id: TransactionId) -> anyhow::Result<Expense> {
        Expense::try_from(ReadTransaction::read(self, id)?)
    }
}

/// Manages the listing of all expenses
pub fn list_expenses_controller<R>(read_expense: &R) -> anyhow::Result<Vec<Expense>>
where
    R: ReadExpense,
{
    let expenses = ReadExpense::read_all(read_expense)?;
    Ok(expenses)
}

#[cfg(any(test, feature = "test-helpers"))]
pub mod mock {

    use chrono::Utc;

    use crate::{test_helpers, MonetaryValue, Transaction, TransactionActor};

    use super::*;

    /// Mock [ReadExpense] object useful for tests
    #[derive(Debug, Clone, Default)]
    pub struct MockReadExpense {
        /// Indicates if the functions should return errors
        pub err: bool,
        /// The array of expenses to be returned by the methods
        pub expenses: Vec<Expense>,
    }

    impl ReadTransaction for MockReadExpense {
        /// Returns the internal vector of cards
        ///
        /// # Errors
        ///
        /// - Internal flag set;
        fn read_all(&self) -> anyhow::Result<Vec<Transaction>> {
            if self.err {
                anyhow::bail!("MockReadExpense internal error flag set");
            }
            Ok(self
                .expenses
                .iter()
                .map(|e| e.transaction())
                .cloned()
                .collect())
        }

        /// Searches the internal vector for the requested expense and returns it
        ///
        /// Returns a random [Expense] object with the requested id if it is not
        /// in the internal vector
        ///
        /// # Errors
        ///
        /// - Internal flag set;
        fn read(&self, id: TransactionId) -> anyhow::Result<Transaction> {
            if self.err {
                anyhow::bail!("MockReadExpense internal error flag set");
            }
            let expense = self
                .expenses
                .iter()
                .cloned()
                .find(|a| a.id() == id)
                .unwrap_or_else(|| {
                    Expense::new(
                        id,
                        Utc::now(),
                        TransactionActor::sample(),
                        MonetaryValue::sample(),
                        Some(test_helpers::random_string(32)),
                    )
                });
            Ok(expense.transaction().clone())
        }
    }

    impl ReadExpense for MockReadExpense {}
}

#[cfg(test)]
mod tests {
    mod controller {
        use crate::MockReadExpense;

        use super::super::*;

        #[test]
        fn should_list_expenses() -> anyhow::Result<()> {
            let expected = vec![Expense::sample(), Expense::sample()];
            let rt = MockReadExpense {
                expenses: expected.clone(),
                ..Default::default()
            };

            let expenses = list_expenses_controller(&rt)?;

            assert_eq!(expected, expenses);
            Ok(())
        }

        #[test]
        fn should_error_if_internal_flag_set() {
            let rt = MockReadExpense {
                err: true,
                ..Default::default()
            };

            let res = list_expenses_controller(&rt);

            assert!(res.is_err());
        }
    }
}
