use crate::TransactionId;

/// Defines the behavior of an object that can delete a Transaction
pub trait DeleteTransaction {
    /// Deletes a transaction from the system
    fn delete(&self, transaction: TransactionId) -> anyhow::Result<()>;
}

/// Manages the deletion of a new transaction
///
/// Simply calls the underlying data storage to delete the transaction
///
/// # Errors
///
/// - Internal;
pub fn delete_transaction_controller<A>(
    delete_transaction: &A,
    transaction: TransactionId,
) -> anyhow::Result<()>
where
    A: DeleteTransaction,
{
    delete_transaction.delete(transaction)?;
    Ok(())
}

#[cfg(any(test, feature = "test-helpers"))]
pub mod mock {
    use super::*;

    /// Mock [DeleteTransaction] object useful for testing
    #[derive(Clone, Debug, Default)]
    pub struct MockDeleteTransaction {
        /// Indicates function should return an error
        pub err: bool,
    }

    impl DeleteTransaction for MockDeleteTransaction {
        /// Default successful response
        ///
        /// # Error
        ///
        /// - Internal flag set;
        fn delete(&self, _transaction: TransactionId) -> anyhow::Result<()> {
            if self.err {
                anyhow::bail!("MockDeleteTransaction internal flag set")
            }
            Ok(())
        }
    }
}

#[cfg(test)]
mod tests {

    mod controller {
        use crate::MockDeleteTransaction;

        use super::super::*;

        #[test]
        fn should_delete_transaction() -> anyhow::Result<()> {
            let dt = MockDeleteTransaction::default();
            let transaction = TransactionId::sample();

            delete_transaction_controller(&dt, transaction)?;

            Ok(())
        }

        #[test]
        fn should_error_if_internal_flag_set() {
            let transaction = TransactionId::sample();
            let dt = MockDeleteTransaction { err: true };

            let res = delete_transaction_controller(&dt, transaction);

            assert!(res.is_err());
        }
    }
}
