use crate::{AddTransaction, Expense};

/// Defines the behavior of an object that can add an Expense
pub trait AddExpense: AddTransaction {
    /// Adds a new expense into the system
    fn add(&self, expense: &Expense) -> anyhow::Result<()> {
        AddTransaction::add(self, expense.transaction())
    }
}

/// Manages the adding of a new expense
///
/// Simply calls the underlying data storage to add the expense
///
/// # Errors
///
/// - Internal;
pub fn add_expense_controller<A>(add_expense: &A, expense: &Expense) -> anyhow::Result<()>
where
    A: AddExpense,
{
    AddExpense::add(add_expense, expense)?;
    Ok(())
}

#[cfg(any(test, feature = "test-helpers"))]
pub mod mock {
    use crate::Transaction;

    use super::*;

    /// Mock [AddExpense] object useful for testing
    #[derive(Clone, Debug, Default)]
    pub struct MockAddExpense {
        /// Indicates function should return an error
        pub err: bool,
    }

    impl AddTransaction for MockAddExpense {
        /// Default successful response
        ///
        /// # Error
        ///
        /// - Internal flag set;
        fn add(&self, _transaction: &Transaction) -> anyhow::Result<()> {
            if self.err {
                anyhow::bail!("MockAddExpense internal flag set")
            }
            Ok(())
        }
    }

    impl AddExpense for MockAddExpense {}
}

#[cfg(test)]
mod tests {

    mod controller {
        use crate::MockAddExpense;

        use super::super::*;

        #[test]
        fn should_add_expense() -> anyhow::Result<()> {
            let ae = MockAddExpense::default();
            let expense = Expense::sample();

            add_expense_controller(&ae, &expense)?;

            Ok(())
        }

        #[test]
        fn should_error_if_internal_flag_set() {
            let expense = Expense::sample();
            let ae = MockAddExpense { err: true };

            let res = add_expense_controller(&ae, &expense);

            assert!(res.is_err());
        }
    }
}
