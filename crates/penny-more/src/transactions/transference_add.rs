use crate::{AddTransaction, Transference};

/// Defines the behavior of an object that can add an Transference
pub trait AddTransference: AddTransaction {
    /// Adds a new transference into the system
    fn add(&self, transference: &Transference) -> anyhow::Result<()> {
        AddTransaction::add(self, transference.transaction())
    }
}

/// Manages the adding of a new transference
///
/// Simply calls the underlying data storage to add the transference
///
/// # Errors
///
/// - Internal;
pub fn add_transference_controller<A>(
    add_transference: &A,
    transference: &Transference,
) -> anyhow::Result<()>
where
    A: AddTransference,
{
    AddTransference::add(add_transference, transference)?;
    Ok(())
}

#[cfg(any(test, feature = "test-helpers"))]
pub mod mock {
    use crate::Transaction;

    use super::*;

    /// Mock [AddTransference] object useful for testing
    #[derive(Clone, Debug, Default)]
    pub struct MockAddTransference {
        /// Indicates function should return an error
        pub err: bool,
    }

    impl AddTransaction for MockAddTransference {
        /// Default successful response
        ///
        /// # Error
        ///
        /// - Internal flag set;
        fn add(&self, _transaction: &Transaction) -> anyhow::Result<()> {
            if self.err {
                anyhow::bail!("MockAddTransference internal flag set")
            }
            Ok(())
        }
    }

    impl AddTransference for MockAddTransference {}
}

#[cfg(test)]
mod tests {

    mod controller {
        use crate::MockAddTransference;

        use super::super::*;

        #[test]
        fn should_add_transference() -> anyhow::Result<()> {
            let at = MockAddTransference::default();
            let transference = Transference::sample();

            add_transference_controller(&at, &transference)?;

            Ok(())
        }

        #[test]
        fn should_error_if_internal_flag_set() {
            let transference = Transference::sample();
            let at = MockAddTransference { err: true };

            let res = add_transference_controller(&at, &transference);

            assert!(res.is_err());
        }
    }
}
