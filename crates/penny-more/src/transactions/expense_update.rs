use chrono::{DateTime, Utc};

use crate::{
    Expense, MonetaryValue, ReadExpense, Transaction, TransactionActor, TransactionId,
    UpdateTransaction,
};

/// Defines the behavior of an object that can update a Transaction
pub trait UpdateExpense: UpdateTransaction {
    /// Updates a transaction in the system
    ///
    /// # Errors
    ///
    /// - Internal;
    fn update(&self, expense: &Expense) -> anyhow::Result<()> {
        UpdateTransaction::update(self, expense.transaction())
    }
}

/// Object that can be used to translate what should be updated in a [Expense]
#[derive(Debug, Clone)]
pub struct ExpenseChangeSet {
    /// Expense that will be changed
    pub id: TransactionId,
    /// New timestamp, [None] means to not update value
    pub timestamp: Option<DateTime<Utc>>,
    /// New owner, [None] means to not update value
    pub owner: Option<TransactionActor>,
    /// New amount, [None] means to not update value
    pub amount: Option<MonetaryValue>,
    /// New description, [None] means to not update value
    pub description: Option<Option<String>>,
}

impl ExpenseChangeSet {
    /// Creates a new [ExpenseChangeSet]
    pub fn new(
        id: TransactionId,
        timestamp: Option<DateTime<Utc>>,
        owner: Option<TransactionActor>,
        amount: Option<MonetaryValue>,
        description: Option<Option<String>>,
    ) -> Self {
        Self {
            id,
            timestamp,
            owner,
            amount,
            description,
        }
    }
}

/// Manages the updating of an expense
///
/// # Errors
///
/// - Expense does not exist;
/// - Internal;
pub fn update_expense_controller<R, U>(
    read_expense: &R,
    upd_expense: &U,
    expense_cs: ExpenseChangeSet,
) -> anyhow::Result<()>
where
    U: UpdateExpense,
    R: ReadExpense,
{
    let saved_expense = ReadExpense::read(read_expense, expense_cs.id)?;

    let id = expense_cs.id;
    let timestamp = expense_cs
        .timestamp
        .unwrap_or_else(|| saved_expense.timestamp());
    let owner = expense_cs.owner.unwrap_or_else(|| saved_expense.owner());
    let amount = expense_cs.amount.unwrap_or_else(|| saved_expense.amount());
    let description = expense_cs
        .description
        .unwrap_or_else(|| saved_expense.description().cloned());
    let expense = Expense::new(id, timestamp, owner, amount, description);

    UpdateExpense::update(upd_expense, &expense)?;

    Ok(())
}

#[cfg(any(test, feature = "test-helpers"))]
pub mod mock {
    use super::*;

    /// Mock [UpdateExpense] object useful for testing
    #[derive(Debug, Default)]
    pub struct MockUpdateExpense {
        /// Holds a clone of the last expense object that was used in an update call
        pub expected_expense: Option<Expense>,
        /// Indicates function should return an error
        pub err: bool,
    }

    impl MockUpdateExpense {
        /// Sets an expected transaction object to be matched in the update method call
        ///
        /// Replaces any existing transaction that was added before
        pub fn set_expected_expense(&mut self, expense: &Expense) {
            self.expected_expense = Some(expense.clone());
        }
    }

    impl UpdateTransaction for MockUpdateExpense {
        /// Default successful response
        ///
        /// # Error
        ///
        /// - Internal error flag set;
        fn update(&self, transaction: &Transaction) -> anyhow::Result<()> {
            if self.err {
                anyhow::bail!("MockDeleteExpense internal error flag set");
            }
            if let Some(exp) = &self.expected_expense {
                if exp.transaction() != transaction {
                    anyhow::bail!("Passed expense is different than expected");
                }
            }
            Ok(())
        }
    }

    impl UpdateExpense for MockUpdateExpense {}
}

#[cfg(test)]
mod tests {

    mod expense_change_set {
        use crate::test_helpers;

        use super::super::*;

        #[test]
        fn should_create_new() {
            let id = TransactionId::sample();
            let timestamp = Some(Utc::now());
            let owner = Some(TransactionActor::sample());
            let amount = Some(MonetaryValue::sample());
            let description = Some(Some(test_helpers::random_string(16)));

            let ecs = ExpenseChangeSet::new(id, timestamp, owner, amount, description.clone());

            assert_eq!(ecs.id, id);
            assert_eq!(ecs.timestamp, timestamp);
            assert_eq!(ecs.owner, owner);
            assert_eq!(ecs.amount, amount);
            assert_eq!(ecs.description, description);
        }
    }

    mod controller {
        use crate::{test_helpers, MockReadExpense, MockUpdateExpense};

        use super::super::*;

        #[test]
        fn should_update_expense() -> anyhow::Result<()> {
            let expense = Expense::sample();
            let re = MockReadExpense {
                expenses: vec![expense.clone()],
                ..Default::default()
            };
            let mut ue = MockUpdateExpense::default();

            // Updating timestamp
            let new_timestamp = Utc::now();
            let expense_cs =
                ExpenseChangeSet::new(expense.id(), Some(new_timestamp), None, None, None);
            let expected_expense = Expense::new(
                expense.id(),
                new_timestamp,
                expense.owner(),
                expense.amount(),
                expense.description().cloned(),
            );
            ue.set_expected_expense(&expected_expense);
            update_expense_controller(&re, &ue, expense_cs)?;

            // Updating owner
            let new_owner = TransactionActor::sample();
            let expense_cs = ExpenseChangeSet::new(expense.id(), None, Some(new_owner), None, None);
            let expected_expense = Expense::new(
                expense.id(),
                expense.timestamp(),
                new_owner,
                expense.amount(),
                expense.description().cloned(),
            );
            ue.set_expected_expense(&expected_expense);
            update_expense_controller(&re, &ue, expense_cs)?;

            // Updating amount
            let new_amount = MonetaryValue::sample();
            let expense_cs =
                ExpenseChangeSet::new(expense.id(), None, None, Some(new_amount), None);
            let expected_expense = Expense::new(
                expense.id(),
                expense.timestamp(),
                expense.owner(),
                new_amount,
                expense.description().cloned(),
            );
            ue.set_expected_expense(&expected_expense);
            update_expense_controller(&re, &ue, expense_cs)?;

            // Updating description
            let new_description = Some(test_helpers::random_string(16));
            let expense_cs = ExpenseChangeSet::new(
                expense.id(),
                None,
                None,
                None,
                Some(new_description.clone()),
            );
            let expected_expense = Expense::new(
                expense.id(),
                expense.timestamp(),
                expense.owner(),
                expense.amount(),
                new_description,
            );
            ue.set_expected_expense(&expected_expense);
            update_expense_controller(&re, &ue, expense_cs)?;

            // Updating nothing
            let expense_cs = ExpenseChangeSet::new(expense.id(), None, None, None, None);
            let expected_expense = expense;
            ue.set_expected_expense(&expected_expense);
            update_expense_controller(&re, &ue, expense_cs)?;

            Ok(())
        }

        #[test]
        fn should_error_if_update_fails() {
            let re = MockReadExpense::default();
            let ue = MockUpdateExpense {
                err: true,
                ..Default::default()
            };
            let expense = Transaction::sample();
            let expense_cs = ExpenseChangeSet::new(expense.id(), None, None, None, None);

            let res = update_expense_controller(&re, &ue, expense_cs);

            assert!(res.is_err());
        }

        #[test]
        fn should_error_if_read_fails() {
            let re = MockReadExpense {
                err: true,
                ..Default::default()
            };
            let ue = MockUpdateExpense::default();
            let expense = Expense::sample();
            let expense_cs = ExpenseChangeSet::new(expense.id(), None, None, None, None);

            let res = update_expense_controller(&re, &ue, expense_cs);

            assert!(res.is_err());
        }
    }
}
