/// Transaction-related types
pub(crate) mod types;

/// Add transaction use case implementation
pub(crate) mod transaction_add;
/// Delete transaction use case implementation
pub(crate) mod transaction_delete;
/// Read transaction use case implementation
pub(crate) mod transaction_read;
/// Update transaction use case implementation
pub(crate) mod transaction_update;

/// Add an earning use case implementation
pub(crate) mod earning_add;
/// Delete an earning use case implementation
pub(crate) mod earning_delete;
/// Read earning use case implementation
pub(crate) mod earning_read;
/// Update earning use case implementation
pub(crate) mod earning_update;

/// Add an expense use case implementation
pub(crate) mod expense_add;
/// Delete an expense use case implementation
pub(crate) mod expense_delete;
/// Read expense use case implementation
pub(crate) mod expense_read;
/// Update expense use case implementation
pub(crate) mod expense_update;

/// Add a transference use case implementation
pub(crate) mod transference_add;
/// Delete a transference use case implementation
pub(crate) mod transference_delete;
/// Read transference use case implementation
pub(crate) mod transference_read;
/// Update transference use case implementation
pub(crate) mod transference_update;
