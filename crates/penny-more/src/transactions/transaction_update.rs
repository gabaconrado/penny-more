use chrono::{DateTime, Utc};

use crate::{MonetaryValue, ReadTransaction, Transaction, TransactionActor, TransactionId};

use super::types::transaction::TransactionType;

/// Defines the behavior of an object that can update a Transaction
pub trait UpdateTransaction {
    /// Updates a transaction in the system
    fn update(&self, transaction: &Transaction) -> anyhow::Result<()>;
}

/// Object that can be used to translate what should be updated in a [Transaction]
#[derive(Debug, Clone)]
pub struct TransactionChangeSet {
    /// Transaction that will be changed
    pub id: TransactionId,
    /// New timestamp, [None] means to not update value
    pub timestamp: Option<DateTime<Utc>>,
    /// New owner, [None] means to not update value
    pub owner: Option<TransactionActor>,
    /// New amount, [None] means to not update value
    pub amount: Option<MonetaryValue>,
    /// New description, [None] means to not update value
    pub description: Option<Option<String>>,
    /// New type of the transaction, [None] means to not update value
    pub transaction_type: Option<TransactionType>,
}

impl TransactionChangeSet {
    /// Creates a new [TransactionChangeSet]
    pub fn new(
        id: TransactionId,
        timestamp: Option<DateTime<Utc>>,
        owner: Option<TransactionActor>,
        amount: Option<MonetaryValue>,
        description: Option<Option<String>>,
        transaction_type: Option<TransactionType>,
    ) -> Self {
        Self {
            id,
            timestamp,
            owner,
            amount,
            description,
            transaction_type,
        }
    }
}

/// Manages the updating of a transaction
///
/// # Errors
///
/// - Transaction does not exist;
/// - Internal;
pub fn update_transaction_controller<R, U>(
    read_transaction: &R,
    upd_transaction: &U,
    transaction_cs: TransactionChangeSet,
) -> anyhow::Result<()>
where
    U: UpdateTransaction,
    R: ReadTransaction,
{
    let saved_transaction = read_transaction.read(transaction_cs.id)?;

    let id = transaction_cs.id;
    let timestamp = transaction_cs
        .timestamp
        .unwrap_or_else(|| saved_transaction.timestamp());
    let owner = transaction_cs
        .owner
        .unwrap_or_else(|| saved_transaction.owner());
    let amount = transaction_cs
        .amount
        .unwrap_or_else(|| saved_transaction.amount());
    let description = transaction_cs
        .description
        .unwrap_or_else(|| saved_transaction.description().cloned());
    let transaction_type = transaction_cs
        .transaction_type
        .unwrap_or_else(|| saved_transaction.transaction_type());
    let transaction = Transaction::new(id, timestamp, owner, amount, description, transaction_type);

    upd_transaction.update(&transaction)?;

    Ok(())
}

#[cfg(any(test, feature = "test-helpers"))]
pub mod mock {
    use super::*;

    /// Mock [UpdateTransaction] object useful for testing
    #[derive(Debug, Default)]
    pub struct MockUpdateTransaction {
        /// Holds a clone of the last transaction object that was used in an update call
        pub expected_transaction: Option<Transaction>,
        /// Indicates function should return an error
        pub err: bool,
    }

    impl MockUpdateTransaction {
        /// Sets an expected transaction object to be matched in the update method call
        ///
        /// Replaces any existing transaction that was added before
        pub fn set_expected_transaction(&mut self, transaction: &Transaction) {
            self.expected_transaction = Some(transaction.clone());
        }
    }

    impl UpdateTransaction for MockUpdateTransaction {
        /// Default successful response
        ///
        /// # Error
        ///
        /// - Internal error flag set;
        fn update(&self, transaction: &Transaction) -> anyhow::Result<()> {
            if self.err {
                anyhow::bail!("MockDeleteTransaction internal error flag set");
            }
            if let Some(exp) = &self.expected_transaction {
                if exp != transaction {
                    anyhow::bail!("Passed transaction is different than expected");
                }
            }
            Ok(())
        }
    }
}

#[cfg(test)]
mod tests {

    mod transaction_change_set {
        use crate::test_helpers;

        use super::super::*;

        #[test]
        fn should_create_new() {
            let id = TransactionId::sample();
            let timestamp = Some(Utc::now());
            let owner = Some(TransactionActor::sample());
            let amount = Some(MonetaryValue::sample());
            let description = Some(Some(test_helpers::random_string(16)));
            let transaction_type = Some(TransactionType::sample());

            let tcs = TransactionChangeSet::new(
                id,
                timestamp,
                owner,
                amount,
                description.clone(),
                transaction_type,
            );

            assert_eq!(tcs.id, id);
            assert_eq!(tcs.timestamp, timestamp);
            assert_eq!(tcs.owner, owner);
            assert_eq!(tcs.amount, amount);
            assert_eq!(tcs.description, description);
            assert_eq!(tcs.transaction_type, transaction_type);
        }
    }

    mod controller {
        use crate::{test_helpers, MockReadTransaction, MockUpdateTransaction};

        use super::super::*;

        #[test]
        fn should_update_transaction() -> anyhow::Result<()> {
            let transaction = Transaction::sample();
            let rt = MockReadTransaction {
                transactions: vec![transaction.clone()],
                ..Default::default()
            };
            let mut ut = MockUpdateTransaction::default();

            // Updating timestamp
            let new_timestamp = Utc::now();
            let transaction_cs = TransactionChangeSet::new(
                transaction.id(),
                Some(new_timestamp),
                None,
                None,
                None,
                None,
            );
            let expected_transaction = Transaction::new(
                transaction.id(),
                new_timestamp,
                transaction.owner(),
                transaction.amount(),
                transaction.description().cloned(),
                transaction.transaction_type(),
            );
            ut.set_expected_transaction(&expected_transaction);
            update_transaction_controller(&rt, &ut, transaction_cs)?;

            // Updating owner
            let new_owner = TransactionActor::sample();
            let transaction_cs = TransactionChangeSet::new(
                transaction.id(),
                None,
                Some(new_owner),
                None,
                None,
                None,
            );
            let expected_transaction = Transaction::new(
                transaction.id(),
                transaction.timestamp(),
                new_owner,
                transaction.amount(),
                transaction.description().cloned(),
                transaction.transaction_type(),
            );
            ut.set_expected_transaction(&expected_transaction);
            update_transaction_controller(&rt, &ut, transaction_cs)?;

            // Updating amount
            let new_amount = MonetaryValue::sample();
            let transaction_cs = TransactionChangeSet::new(
                transaction.id(),
                None,
                None,
                Some(new_amount),
                None,
                None,
            );
            let expected_transaction = Transaction::new(
                transaction.id(),
                transaction.timestamp(),
                transaction.owner(),
                new_amount,
                transaction.description().cloned(),
                transaction.transaction_type(),
            );
            ut.set_expected_transaction(&expected_transaction);
            update_transaction_controller(&rt, &ut, transaction_cs)?;

            // Updating description
            let new_description = Some(test_helpers::random_string(16));
            let transaction_cs = TransactionChangeSet::new(
                transaction.id(),
                None,
                None,
                None,
                Some(new_description.clone()),
                None,
            );
            let expected_transaction = Transaction::new(
                transaction.id(),
                transaction.timestamp(),
                transaction.owner(),
                transaction.amount(),
                new_description,
                transaction.transaction_type(),
            );
            ut.set_expected_transaction(&expected_transaction);
            update_transaction_controller(&rt, &ut, transaction_cs)?;

            // Updating type
            let new_transaction_type = TransactionType::sample();
            let transaction_cs = TransactionChangeSet::new(
                transaction.id(),
                None,
                None,
                None,
                None,
                Some(new_transaction_type),
            );
            let expected_transaction = Transaction::new(
                transaction.id(),
                transaction.timestamp(),
                transaction.owner(),
                transaction.amount(),
                transaction.description().cloned(),
                new_transaction_type,
            );
            ut.set_expected_transaction(&expected_transaction);
            update_transaction_controller(&rt, &ut, transaction_cs)?;

            // Updating nothing
            let transaction_cs =
                TransactionChangeSet::new(transaction.id(), None, None, None, None, None);
            let expected_transaction = transaction;
            ut.set_expected_transaction(&expected_transaction);
            update_transaction_controller(&rt, &ut, transaction_cs)?;

            Ok(())
        }

        #[test]
        fn should_error_if_update_fails() {
            let rt = MockReadTransaction::default();
            let ut = MockUpdateTransaction {
                err: true,
                ..Default::default()
            };
            let transaction = Transaction::sample();
            let transaction_cs =
                TransactionChangeSet::new(transaction.id(), None, None, None, None, None);

            let res = update_transaction_controller(&rt, &ut, transaction_cs);

            assert!(res.is_err());
        }

        #[test]
        fn should_error_if_read_fails() {
            let rt = MockReadTransaction {
                err: true,
                ..Default::default()
            };
            let ut = MockUpdateTransaction::default();
            let transaction = Transaction::sample();
            let transaction_cs =
                TransactionChangeSet::new(transaction.id(), None, None, None, None, None);

            let res = update_transaction_controller(&rt, &ut, transaction_cs);

            assert!(res.is_err());
        }
    }
}
