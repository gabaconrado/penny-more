use crate::{ReadTransaction, TransactionId, Transference};

/// Defines the behavior of an object that can read stored Transferences
pub trait ReadTransference: ReadTransaction {
    /// Reads all saved transferences
    ///
    /// # Errors
    ///
    /// - Error parsing any transaction;
    /// - Internal;
    fn read_all(&self) -> anyhow::Result<Vec<Transference>> {
        ReadTransaction::read_all(self)?
            .into_iter()
            .map(Transference::try_from)
            .collect()
    }

    /// Reads an Transference by its ID
    ///
    /// Calls the internal read transaction
    ///
    /// # Errors
    ///
    /// - Transference does not exist;
    /// - Error parsing transaction;
    fn read(&self, id: TransactionId) -> anyhow::Result<Transference> {
        Transference::try_from(ReadTransaction::read(self, id)?)
    }
}

/// Manages the listing of all transferences
pub fn list_transferences_controller<R>(read_transference: &R) -> anyhow::Result<Vec<Transference>>
where
    R: ReadTransference,
{
    let transferences = ReadTransference::read_all(read_transference)?;
    Ok(transferences)
}

#[cfg(any(test, feature = "test-helpers"))]
pub mod mock {

    use chrono::Utc;

    use crate::{test_helpers, MonetaryValue, Transaction, TransactionActor};

    use super::*;

    /// Mock [ReadTransference] object useful for tests
    #[derive(Debug, Clone, Default)]
    pub struct MockReadTransference {
        /// Indicates if the functions should return errors
        pub err: bool,
        /// The array of transferences to be returned by the methods
        pub transferences: Vec<Transference>,
    }

    impl ReadTransaction for MockReadTransference {
        /// Returns the internal vector of cards
        ///
        /// # Errors
        ///
        /// - Internal flag set;
        fn read_all(&self) -> anyhow::Result<Vec<Transaction>> {
            if self.err {
                anyhow::bail!("MockReadTransference internal error flag set");
            }
            Ok(self
                .transferences
                .iter()
                .map(|e| e.transaction())
                .cloned()
                .collect())
        }

        /// Searches the internal vector for the requested transference and returns it
        ///
        /// Returns a random [Transference] object with the requested id if it is not
        /// in the internal vector
        ///
        /// # Errors
        ///
        /// - Internal flag set;
        fn read(&self, id: TransactionId) -> anyhow::Result<Transaction> {
            if self.err {
                anyhow::bail!("MockReadTransference internal error flag set");
            }
            let transference = self
                .transferences
                .iter()
                .cloned()
                .find(|a| a.id() == id)
                .unwrap_or_else(|| {
                    Transference::new(
                        id,
                        Utc::now(),
                        TransactionActor::sample(),
                        TransactionActor::sample(),
                        MonetaryValue::sample(),
                        Some(test_helpers::random_string(32)),
                    )
                });
            Ok(transference.transaction().clone())
        }
    }

    impl ReadTransference for MockReadTransference {}
}

#[cfg(test)]
mod tests {
    mod controller {
        use crate::MockReadTransference;

        use super::super::*;

        #[test]
        fn should_list_transferences() -> anyhow::Result<()> {
            let expected = vec![Transference::sample(), Transference::sample()];
            let rt = MockReadTransference {
                transferences: expected.clone(),
                ..Default::default()
            };

            let transferences = list_transferences_controller(&rt)?;

            assert_eq!(expected, transferences);
            Ok(())
        }

        #[test]
        fn should_error_if_internal_flag_set() {
            let rt = MockReadTransference {
                err: true,
                ..Default::default()
            };

            let res = list_transferences_controller(&rt);

            assert!(res.is_err());
        }
    }
}
