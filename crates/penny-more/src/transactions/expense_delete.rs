use crate::{DeleteTransaction, TransactionId};

/// Defines the behavior of an object that can delete an Expense
pub trait DeleteExpense: DeleteTransaction {
    /// Deletes an expense from the system
    ///
    /// Default implementation calls the [DeleteTransaction] implementation
    fn delete(&self, expense: TransactionId) -> anyhow::Result<()> {
        DeleteTransaction::delete(self, expense)
    }
}

/// Manages the deletion of an Expense
///
/// Simply calls the underlying data storage to remove the expense
///
/// # Errors
///
/// - Internal;
pub fn delete_expense_controller<D>(
    delete_earning: &D,
    expense: TransactionId,
) -> anyhow::Result<()>
where
    D: DeleteExpense,
{
    DeleteExpense::delete(delete_earning, expense)?;
    Ok(())
}

#[cfg(any(test, feature = "test-helpers"))]
pub mod mock {
    use super::*;

    /// Mock [DeleteEarning] object useful for testing
    #[derive(Clone, Debug, Default)]
    pub struct MockDeleteExpense {
        /// Indicates function should return an error
        pub err: bool,
    }

    impl DeleteTransaction for MockDeleteExpense {
        /// Default successful response
        ///
        /// # Error
        ///
        /// - Internal flag set;
        fn delete(&self, _transaction: TransactionId) -> anyhow::Result<()> {
            if self.err {
                anyhow::bail!("MockDeleteExpense internal flag set")
            }
            Ok(())
        }
    }

    impl DeleteExpense for MockDeleteExpense {}
}

#[cfg(test)]
mod tests {

    mod controller {
        use crate::MockDeleteExpense;

        use super::super::*;

        #[test]
        fn should_delete_transaction() -> anyhow::Result<()> {
            let dt = MockDeleteExpense::default();
            let expense = TransactionId::sample();

            delete_expense_controller(&dt, expense)?;

            Ok(())
        }

        #[test]
        fn should_error_if_internal_flag_set() {
            let dt = MockDeleteExpense { err: true };
            let expense = TransactionId::sample();

            let res = delete_expense_controller(&dt, expense);

            assert!(res.is_err());
        }
    }
}
