use rand::{distributions::Alphanumeric, Rng};

/// Creates a random string for the given length
pub fn random_string(len: usize) -> String {
    rand::thread_rng()
        .sample_iter(&Alphanumeric)
        .take(len)
        .map(char::from)
        .collect()
}
