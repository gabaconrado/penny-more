#![doc = include_str!("../README.md")]
#![deny(
    missing_debug_implementations,
    missing_docs,
    trivial_casts,
    trivial_numeric_casts,
    unused_extern_crates,
    unused_results,
    clippy::unwrap_used,
    clippy::as_conversions,
    clippy::indexing_slicing
)]

use clap::Parser;
use command::account;
use tracing_subscriber::util::SubscriberInitExt;

use crate::argument::{AccountCommand, Objects};

/// Clap argument parsing
mod argument;
/// Commands implementations
mod command;

fn main() -> anyhow::Result<()> {
    let cli = argument::Cli::parse();
    setup_tracing(cli.verbosity);
    tracing::info!("Starting Penny-more CLI");
    match cli.object {
        Objects::Account(cmd) => run_account_command(cmd)?,
    };
    tracing::info!("Finishing Penny-more CLI");
    Ok(())
}

fn setup_tracing(verbosity: u8) {
    use tracing::Level;
    let level = match verbosity {
        1 => Level::INFO,
        2 => Level::DEBUG,
        3 => Level::TRACE,
        _ => Level::WARN,
    };
    tracing_subscriber::fmt()
        .with_max_level(level)
        .finish()
        .init();
}

#[tracing::instrument]
fn run_account_command(cmd: AccountCommand) -> anyhow::Result<()> {
    match cmd {
        AccountCommand::List => account::list::list_accounts_cmd()?,
        AccountCommand::Add { name, balance } => account::add::add_account_cmd(name, balance)?,
        AccountCommand::Delete { id } => account::delete::delete_account_cmd(id)?,
        AccountCommand::Update { id, name, balance } => {
            account::update::update_account_cmd(id, name, balance)?
        }
    }
    Ok(())
}
