use clap::{Parser, Subcommand};
use penny_more::{AccountId, CardId, MonetaryValue};

/// The Penny-more command line interface
#[derive(Debug, Parser)]
#[command(author, version, about)]
pub struct Cli {
    /// Object to act on
    #[command(subcommand)]
    pub object: Objects,
    /// Verbosity of the CLI
    #[arg(short, long, action = clap::ArgAction::Count)]
    pub verbosity: u8,
}

/// All objects for the CLI
#[derive(Debug, Subcommand)]
pub enum Objects {
    /// Commands on accounts
    #[command(subcommand)]
    Account(AccountCommand),
}

/// All commands related to accounts
#[derive(Debug, Subcommand)]
pub enum AccountCommand {
    /// List all saved accounts
    List,
    /// Adds a new account to the system
    Add {
        /// The name of the new account
        #[arg(short, long)]
        name: String,
        /// The initial balance of the new account
        #[arg(short, long)]
        balance: MonetaryValue,
    },
    /// Updates a saved account
    Update {
        /// The Id of the account to be changed
        #[arg(short, long)]
        id: AccountId,
        /// The new name of the account
        #[arg(short, long)]
        name: Option<String>,
        /// The new account balance
        #[arg(short, long)]
        balance: Option<MonetaryValue>,
    },
    /// Deletes a saved account
    Delete {
        /// The Id of the account to be deleted
        #[arg(short, long)]
        id: AccountId,
    },
}

/// All commands related to cards
#[derive(Debug, Subcommand)]
pub enum CardCommand {
    /// List all saved cards
    List,
    /// Adds a new card to the system
    Add {
        /// The name of the new card
        #[arg(short, long)]
        name: String,
        /// The limit of the new card
        #[arg(short, long)]
        limit: MonetaryValue,
        /// A related account to the card
        #[arg(short, long)]
        account: Option<AccountId>,
    },
    /// Updates a saved card
    Update {
        /// The Id of the card to be changed
        #[arg(short, long)]
        id: CardId,
        /// The new name for the card
        #[arg(short, long)]
        name: Option<String>,
        /// The new limit of the card
        #[arg(short, long)]
        balance: Option<MonetaryValue>,
        /// The new account to the card
        #[arg(short, long)]
        account: Option<AccountId>,
    },
    /// Deletes a saved account
    Delete {
        /// The Id of the account to be deleted
        #[arg(short, long)]
        id: AccountId,
    },
}
