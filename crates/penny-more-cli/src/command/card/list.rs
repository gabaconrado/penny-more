use data_storage::InMemoryDatabase;

/// List all saved cards
#[tracing::instrument]
pub fn list_cards_cmd() -> anyhow::Result<()> {
    let storage = InMemoryDatabase::default();
    tracing::debug!("Getting all cards from storage");

    let cards = penny_more::list_cards_controller(&storage)?;
    tracing::debug!("Returned cards from library: {cards:?}");

    let output = format!("Saved cards: {cards:?}");
    println!("{output}");
    Ok(())
}
