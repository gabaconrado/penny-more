use data_storage::InMemoryDatabase;
use penny_more::{AccountId, Card, CardId, MonetaryValue};

/// Calls controller to add a card
#[tracing::instrument]
pub fn add_card_cmd(
    name: String,
    limit: MonetaryValue,
    account: Option<AccountId>,
) -> anyhow::Result<()> {
    let storage = InMemoryDatabase::default();
    let new_id = CardId::random();
    let card = Card::new(new_id, name, limit, account);
    penny_more::add_card_controller(&storage, &storage, &card)?;
    let output = format!("Added card to system: {new_id} | {limit}");
    println!("{output}");
    Ok(())
}
