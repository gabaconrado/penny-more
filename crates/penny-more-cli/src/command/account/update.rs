use data_storage::InMemoryDatabase;
use penny_more::{AccountChangeSet, AccountId, MonetaryValue};

/// Calls controller to update an account
#[tracing::instrument]
pub fn update_account_cmd(
    id: AccountId,
    name: Option<String>,
    balance: Option<MonetaryValue>,
) -> anyhow::Result<()> {
    let storage = InMemoryDatabase::default();
    let account_cs = AccountChangeSet::new(id, name, balance);
    tracing::debug!("Calling controller with change set: {account_cs:?}");

    penny_more::update_account_controller(&storage, &storage, account_cs)?;

    let output = "Account updated".to_string();
    println!("{output}");
    Ok(())
}
