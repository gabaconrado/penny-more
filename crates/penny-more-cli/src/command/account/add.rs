use data_storage::InMemoryDatabase;
use penny_more::{Account, AccountId, MonetaryValue};

/// Calls controller to add an account
#[tracing::instrument]
pub fn add_account_cmd(name: String, initial_balance: MonetaryValue) -> anyhow::Result<()> {
    let storage = InMemoryDatabase::default();
    let new_id = AccountId::random();
    let account = Account::new(new_id, name.clone(), initial_balance);
    penny_more::add_account_controller(&storage, &account)?;
    let output = format!("Added account to system: {new_id} | {name} | {initial_balance}");
    println!("{output}");
    Ok(())
}
