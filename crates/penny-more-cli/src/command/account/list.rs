use data_storage::InMemoryDatabase;

/// Lists all saved accounts
#[tracing::instrument]
pub fn list_accounts_cmd() -> anyhow::Result<()> {
    let storage = InMemoryDatabase::default();
    tracing::debug!("Getting all accounts from controller");

    let accounts = penny_more::list_accounts_controller(&storage)?;
    tracing::debug!("Returned accounts from library: {accounts:?}");

    let output = format!("Saved accounts: {accounts:?}");
    println!("{output}");
    Ok(())
}
