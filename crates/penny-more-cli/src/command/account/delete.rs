use data_storage::InMemoryDatabase;
use penny_more::AccountId;

/// Calls controller to delete an account
#[tracing::instrument]
pub fn delete_account_cmd(id: AccountId) -> anyhow::Result<()> {
    let storage = InMemoryDatabase::default();
    tracing::debug!("Calling controller to delete account: {id}");

    penny_more::delete_account_controller(&storage, id)?;

    let output = format!("Account {id} deleted");
    println!("{output}");
    Ok(())
}
