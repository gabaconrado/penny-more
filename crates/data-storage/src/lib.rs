#![doc = include_str!("../README.md")]
#![deny(
    missing_debug_implementations,
    missing_docs,
    trivial_casts,
    trivial_numeric_casts,
    unused_extern_crates,
    unused_results,
    clippy::unwrap_used,
    clippy::as_conversions,
    clippy::indexing_slicing
)]

/// Simple in-memory data storage implementations
mod in_memory;

pub use self::in_memory::{database::InMemoryDatabase, error::Error as InMemoryDatabaseError};
