use penny_more::{Card, CardId};

use super::error::Error;
use crate::InMemoryDatabase;

impl InMemoryDatabase {
    /// Inserts or updates an existing card
    ///
    /// # Errors
    ///
    /// - Internal lock poisoned;
    pub fn add_or_update_card(&self, card: &Card) -> Result<bool, Error> {
        let updated = self.cards.lock()?.insert(card.id(), card.clone());
        Ok(updated.is_none())
    }

    /// Reads all cards from the internal map
    ///
    /// # Errors
    ///
    /// - Internal lock poisoned;
    pub fn get_all_cards(&self) -> Result<Vec<Card>, Error> {
        let cards = self.cards.lock()?.values().cloned().collect::<Vec<Card>>();
        Ok(cards)
    }

    /// Returns an card by its identifier from the internal map
    ///
    /// # Errors
    ///
    /// - Card does not exist;
    /// - Internal lock poisoned;
    pub fn get_card_by_id(&self, id: CardId) -> Result<Card, Error> {
        let card = self
            .cards
            .lock()?
            .get(&id)
            .ok_or(Error::CardDoesNotExist(id))?
            .clone();
        Ok(card)
    }

    /// Delete an card from the internal map
    ///
    /// Returns true if any card was deleted, false otherwise
    ///
    /// # Errors
    ///
    /// - Internal lock poisoned;
    pub fn delete_card(&self, id: CardId) -> Result<bool, Error> {
        let removed = self.cards.lock()?.remove(&id);
        Ok(removed.is_some())
    }
}

#[cfg(test)]
pub(crate) mod tests {
    use super::*;

    mod get {
        use super::*;

        #[test]
        fn should_get_all() -> Result<(), Error> {
            // Setup
            let db = InMemoryDatabase::default();
            let cards = [Card::sample(), Card::sample()];
            cards.iter().for_each(|a| add_card_to_db(&db, a));

            // Test
            let all = db.get_all_cards()?;

            // Assertions
            assert!(all.iter().all(|a| cards.contains(a)));
            Ok(())
        }

        #[test]
        fn should_get_by_id() -> Result<(), Error> {
            // Setup
            let db = InMemoryDatabase::default();
            let card = Card::sample();
            add_card_to_db(&db, &card);

            // Test
            let acc = db.get_card_by_id(card.id())?;

            // Assertions
            assert_eq!(acc, card);
            Ok(())
        }

        #[test]
        fn should_error_if_does_not_exist() {
            // Setup
            let db = InMemoryDatabase::default();
            let id = CardId::sample();

            // Test
            let res = db.get_card_by_id(id);

            // Assertions
            assert!(matches!(res, Err(Error::CardDoesNotExist(i)) if i == id));
        }
    }

    mod add_or_update {
        use penny_more::{test_helpers, AccountId, MonetaryValue};

        use super::*;

        #[test]
        fn should_add() -> Result<(), Error> {
            // Setup
            let db = InMemoryDatabase::default();
            let card = Card::sample();

            // Test
            let added = db.add_or_update_card(&card)?;

            // Assertions
            let acc = get_card_from_db(&db, card.id());
            assert!(added);
            assert_eq!(acc, card);
            Ok(())
        }

        #[test]
        fn should_update() -> Result<(), Error> {
            // Setup
            let db = InMemoryDatabase::default();
            let card = Card::sample();
            add_card_to_db(&db, &card);
            let (new_name, new_limit, new_account) = (
                test_helpers::random_string(16),
                MonetaryValue::sample(),
                Some(AccountId::sample()),
            );
            let updated = Card::new(card.id(), new_name, new_limit, new_account);

            // Test
            let added = db.add_or_update_card(&updated)?;

            // Assertions
            let acc = get_card_from_db(&db, card.id());
            assert!(!added);
            assert_eq!(acc, updated);
            Ok(())
        }
    }

    mod delete {
        use super::*;

        #[test]
        fn should_delete() -> Result<(), Error> {
            // Setup
            let db = InMemoryDatabase::default();
            let card = Card::sample();
            add_card_to_db(&db, &card);

            // Test
            let deleted = db.delete_card(card.id())?;
            let not_deleted = db.delete_card(CardId::sample())?;

            // Assertions
            assert!(deleted);
            assert!(!not_deleted);
            check_card_not_in_db(&db, card.id());
            Ok(())
        }
    }

    /// Adds an card to the db for testing
    pub(crate) fn add_card_to_db(db: &InMemoryDatabase, card: &Card) {
        let added = db
            .cards
            .lock()
            .expect("Could not lock database mutex")
            .insert(card.id(), card.clone());
        assert!(added.is_none());
    }

    /// Gets an card from the db for testing
    pub(crate) fn get_card_from_db(db: &InMemoryDatabase, id: CardId) -> Card {
        let card = db
            .cards
            .lock()
            .expect("Could not lock database mutex")
            .get(&id)
            .expect("Card does not exist in the database")
            .clone();
        card
    }

    /// Asserts that the passed card is not in the database
    pub(crate) fn check_card_not_in_db(db: &InMemoryDatabase, id: CardId) {
        let not_in_db = db
            .cards
            .lock()
            .expect("Could not lock database mutex")
            .get(&id)
            .is_none();
        assert!(not_in_db);
    }
}
