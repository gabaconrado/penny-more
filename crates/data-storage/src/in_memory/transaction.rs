use penny_more::{Transaction, TransactionId};

use super::error::Error;
use crate::InMemoryDatabase;

impl InMemoryDatabase {
    /// Inserts or updates an existing transaction
    ///
    /// Returns true if transaction was created, false otherwise
    ///
    /// # Errors
    ///
    /// - Internal lock poisoned;
    pub fn add_or_update_transaction(&self, transaction: &Transaction) -> Result<bool, Error> {
        let updated = self
            .transactions
            .lock()?
            .insert(transaction.id(), transaction.clone());
        Ok(updated.is_none())
    }

    /// Reads all transactions from the internal map
    ///
    /// # Errors
    ///
    /// - Internal lock poisoned;
    pub fn get_all_transactions(&self) -> Result<Vec<Transaction>, Error> {
        let transactions = self
            .transactions
            .lock()?
            .values()
            .cloned()
            .collect::<Vec<Transaction>>();
        Ok(transactions)
    }

    /// Returns a transaction by its identifier from the internal map
    ///
    /// # Errors
    ///
    /// - Transaction does not exist;
    /// - Internal lock poisoned;
    pub fn get_transaction_by_id(&self, id: TransactionId) -> Result<Transaction, Error> {
        let transaction = self
            .transactions
            .lock()?
            .get(&id)
            .ok_or(Error::TransactionDoesNotExist(id))?
            .clone();
        Ok(transaction)
    }

    /// Delete a transaction from the internal map
    ///
    /// Returns true if any transaction was deleted, false otherwise
    ///
    /// # Errors
    ///
    /// - Internal lock poisoned;
    pub fn delete_transaction(&self, id: TransactionId) -> Result<bool, Error> {
        let removed = self.transactions.lock()?.remove(&id);
        Ok(removed.is_some())
    }
}

#[cfg(test)]
pub(crate) mod tests {

    use super::*;

    mod add_or_update {
        use super::*;

        #[test]
        fn should_add() -> Result<(), Error> {
            let db = InMemoryDatabase::default();
            let transaction = Transaction::sample();

            let added = db.add_or_update_transaction(&transaction)?;

            let tr = get_transaction_from_db(&db, transaction.id());
            assert!(added);
            assert_eq!(tr, transaction);
            Ok(())
        }

        #[test]
        fn should_update() -> Result<(), Error> {
            let db = InMemoryDatabase::default();
            let transaction = Transaction::sample();
            add_transaction_to_db(&db, &transaction);

            let updated = Transaction::sample_with_id(transaction.id());
            let added = db.add_or_update_transaction(&updated)?;

            let tr = get_transaction_from_db(&db, transaction.id());
            assert!(!added);
            assert_eq!(tr, updated);
            Ok(())
        }
    }

    mod get {
        use super::*;

        #[test]
        fn should_get_all() -> Result<(), Error> {
            let db = InMemoryDatabase::default();
            let transactions = [Transaction::sample(), Transaction::sample()];
            transactions
                .iter()
                .for_each(|t| add_transaction_to_db(&db, t));

            let all = db.get_all_transactions()?;

            assert!(all.iter().all(|t| transactions.contains(t)));
            Ok(())
        }

        #[test]
        fn should_get_by_id() -> Result<(), Error> {
            let db = InMemoryDatabase::default();
            let transaction = Transaction::sample();
            add_transaction_to_db(&db, &transaction);

            let t = db.get_transaction_by_id(transaction.id())?;

            assert_eq!(t, transaction);
            Ok(())
        }

        #[test]
        fn should_error_if_does_not_exist() {
            let db = InMemoryDatabase::default();
            let id = TransactionId::sample();

            let res = db.get_transaction_by_id(id);

            assert!(matches!(res, Err(Error::TransactionDoesNotExist(i)) if i == id));
        }
    }

    mod delete {
        use super::*;

        #[test]
        fn should_delete() -> Result<(), Error> {
            let db = InMemoryDatabase::default();
            let transaction = Transaction::sample();
            add_transaction_to_db(&db, &transaction);

            let deleted = db.delete_transaction(transaction.id())?;
            let not_deleted = db.delete_transaction(TransactionId::sample())?;

            assert!(deleted);
            assert!(!not_deleted);
            check_transaction_not_in_db(&db, transaction.id());
            Ok(())
        }
    }

    /// Adds a transaction to the db for testing
    pub(crate) fn add_transaction_to_db(db: &InMemoryDatabase, tr: &Transaction) {
        let added = db
            .transactions
            .lock()
            .expect("Could not lock database mutex")
            .insert(tr.id(), tr.clone());
        assert!(added.is_none());
    }

    /// Gets a transaction from the db for testing
    pub(crate) fn get_transaction_from_db(db: &InMemoryDatabase, id: TransactionId) -> Transaction {
        let transaction = db
            .transactions
            .lock()
            .expect("Could not lock database mutex")
            .get(&id)
            .expect("Transaction does not exist in the database")
            .clone();
        transaction
    }

    /// Asserts that the passed transaction is not in the database
    pub(crate) fn check_transaction_not_in_db(db: &InMemoryDatabase, id: TransactionId) {
        let not_in_db = db
            .transactions
            .lock()
            .expect("Could not lock database mutex")
            .get(&id)
            .is_none();
        assert!(not_in_db);
    }
}
