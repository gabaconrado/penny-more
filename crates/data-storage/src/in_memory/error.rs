use penny_more::{AccountId, CardId, TransactionId};

/// All errors returned by the In-memory data storage
#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Error {
    /// Account does not exist
    AccountDoesNotExist(AccountId),
    /// Card does not exist
    CardDoesNotExist(CardId),
    /// Transaction does not exist
    TransactionDoesNotExist(TransactionId),
    /// Poisoned lock
    PoisonedLock(String),
}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Error::PoisonedLock(ctx) => write!(f, "Poisoned lock: {ctx}"),
            Error::AccountDoesNotExist(id) => write!(f, "Requested account does not exist: {id}"),
            Error::CardDoesNotExist(id) => write!(f, "Requested card does not exist: {id}"),
            Error::TransactionDoesNotExist(id) => {
                write!(f, "Requested transaction does not exist: {id}")
            }
        }
    }
}

impl std::error::Error for Error {}

impl<T> From<std::sync::PoisonError<T>> for Error {
    fn from(err: std::sync::PoisonError<T>) -> Self {
        Error::PoisonedLock(err.to_string())
    }
}
