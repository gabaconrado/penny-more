use penny_more::{AddTransference, DeleteTransference, ReadTransference, UpdateTransference};

use crate::InMemoryDatabase;

impl AddTransference for InMemoryDatabase {}

impl ReadTransference for InMemoryDatabase {}

impl UpdateTransference for InMemoryDatabase {}

impl DeleteTransference for InMemoryDatabase {}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::in_memory::transaction::tests::{
        add_transaction_to_db, check_transaction_not_in_db, get_transaction_from_db,
    };
    use penny_more::{TransactionId, Transference};

    mod add {
        use super::*;

        #[test]
        fn should_add_transference() -> anyhow::Result<()> {
            // Setup
            let db = InMemoryDatabase::default();
            let transference = Transference::sample();
            check_transaction_not_in_db(&db, transference.id());

            // Test
            db.add(&transference)?;

            // Assertions
            let tr = get_transaction_from_db(&db, transference.id());
            assert_eq!(tr, *transference.transaction());
            Ok(())
        }

        #[test]
        fn should_error_if_exists() {
            // Setup
            let db = InMemoryDatabase::default();
            let transference = Transference::sample();
            add_transaction_to_db(&db, transference.transaction());

            // Test
            let res = db.add(&transference);

            // Assertions
            assert!(matches!(res, Err(err) if err.to_string() == "Transaction already exists"));
        }
    }

    mod read {

        use super::*;

        #[test]
        fn should_read_all() -> anyhow::Result<()> {
            // Setup
            let db = InMemoryDatabase::default();
            let transference = Transference::sample();
            add_transaction_to_db(&db, transference.transaction());

            // Test
            let transferences = db.read_all()?;

            // Assertions
            assert!(matches!(transferences.as_slice(), [e] if *e == transference));
            Ok(())
        }

        #[test]
        fn should_read() -> anyhow::Result<()> {
            // Setup
            let db = InMemoryDatabase::default();
            let transference = Transference::sample();
            add_transaction_to_db(&db, transference.transaction());

            // Test
            let saved = db.read(transference.id())?;

            // Assertions
            assert_eq!(transference, saved);
            Ok(())
        }

        #[test]
        fn read_should_fail_if_transference_does_not_exist() {
            // Setup
            let db = InMemoryDatabase::default();

            // Test
            let res = db.read(TransactionId::sample());
            println!("{res:?}");

            // Assertions
            assert!(
                matches!(res, Err(err) if err.to_string().contains("Requested transaction does not exist"))
            );
        }
    }

    mod update {
        use super::*;

        #[test]
        fn should_update() -> anyhow::Result<()> {
            // Setup
            let db = InMemoryDatabase::default();
            let transference = Transference::sample();
            add_transaction_to_db(&db, transference.transaction());
            let new_transference = Transference::sample_with_id(transference.id());

            // Test
            db.update(&new_transference)?;

            // Assertions
            let in_db = get_transaction_from_db(&db, new_transference.id());
            assert_eq!(in_db, *new_transference.transaction());
            Ok(())
        }

        #[test]
        fn should_error_if_does_not_exist() {
            // Setup
            let db = InMemoryDatabase::default();
            let transference = Transference::sample();

            // Test
            let res = db.update(&transference);

            // Assertions
            assert!(
                matches!(res, Err(err) if err.to_string().starts_with("Transaction does not exist"))
            );
        }
    }

    mod delete {
        use super::*;

        #[test]
        fn should_delete() -> anyhow::Result<()> {
            // Setup
            let db = InMemoryDatabase::default();
            let transference = Transference::sample();
            add_transaction_to_db(&db, transference.transaction());

            // Test
            DeleteTransference::delete(&db, transference.id())?;

            // Assertions
            check_transaction_not_in_db(&db, transference.id());
            Ok(())
        }

        #[test]
        fn should_error_if_does_not_exist() {
            // Setup
            let db = InMemoryDatabase::default();
            let transference = Transference::sample();

            // Test
            let res = DeleteTransference::delete(&db, transference.id());

            // Assertions
            assert!(
                matches!(res, Err(err) if err.to_string().starts_with("Transaction does not exist"))
            );
        }
    }
}
