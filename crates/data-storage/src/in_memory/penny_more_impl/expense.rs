use penny_more::{AddExpense, DeleteExpense, ReadExpense, UpdateExpense};

use crate::InMemoryDatabase;

impl AddExpense for InMemoryDatabase {}

impl ReadExpense for InMemoryDatabase {}

impl UpdateExpense for InMemoryDatabase {}

impl DeleteExpense for InMemoryDatabase {}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::in_memory::transaction::tests::{
        add_transaction_to_db, check_transaction_not_in_db, get_transaction_from_db,
    };
    use penny_more::{Expense, TransactionId};

    mod add {
        use super::*;

        #[test]
        fn should_add_expense() -> anyhow::Result<()> {
            // Setup
            let db = InMemoryDatabase::default();
            let expense = Expense::sample();
            check_transaction_not_in_db(&db, expense.id());

            // Test
            db.add(&expense)?;

            // Assertions
            let tr = get_transaction_from_db(&db, expense.id());
            assert_eq!(tr, *expense.transaction());
            Ok(())
        }

        #[test]
        fn should_error_if_exists() {
            // Setup
            let db = InMemoryDatabase::default();
            let expense = Expense::sample();
            add_transaction_to_db(&db, expense.transaction());

            // Test
            let res = db.add(&expense);

            // Assertions
            assert!(matches!(res, Err(err) if err.to_string() == "Transaction already exists"));
        }
    }

    mod read {

        use super::*;

        #[test]
        fn should_read_all() -> anyhow::Result<()> {
            // Setup
            let db = InMemoryDatabase::default();
            let expense = Expense::sample();
            add_transaction_to_db(&db, expense.transaction());

            // Test
            let expenses = db.read_all()?;

            // Assertions
            assert!(matches!(expenses.as_slice(), [e] if *e == expense));
            Ok(())
        }

        #[test]
        fn should_read() -> anyhow::Result<()> {
            // Setup
            let db = InMemoryDatabase::default();
            let expense = Expense::sample();
            add_transaction_to_db(&db, expense.transaction());

            // Test
            let saved = db.read(expense.id())?;

            // Assertions
            assert_eq!(expense, saved);
            Ok(())
        }

        #[test]
        fn read_should_fail_if_expense_does_not_exist() {
            // Setup
            let db = InMemoryDatabase::default();

            // Test
            let res = db.read(TransactionId::sample());
            println!("{res:?}");

            // Assertions
            assert!(
                matches!(res, Err(err) if err.to_string().contains("Requested transaction does not exist"))
            );
        }
    }

    mod update {
        use super::*;

        #[test]
        fn should_update() -> anyhow::Result<()> {
            // Setup
            let db = InMemoryDatabase::default();
            let expense = Expense::sample();
            add_transaction_to_db(&db, expense.transaction());
            let new_expense = Expense::sample_with_id(expense.id());

            // Test
            db.update(&new_expense)?;

            // Assertions
            let in_db = get_transaction_from_db(&db, new_expense.id());
            assert_eq!(in_db, *new_expense.transaction());
            Ok(())
        }

        #[test]
        fn should_error_if_does_not_exist() {
            // Setup
            let db = InMemoryDatabase::default();
            let expense = Expense::sample();

            // Test
            let res = db.update(&expense);

            // Assertions
            assert!(
                matches!(res, Err(err) if err.to_string().starts_with("Transaction does not exist"))
            );
        }
    }

    mod delete {
        use super::*;

        #[test]
        fn should_delete() -> anyhow::Result<()> {
            // Setup
            let db = InMemoryDatabase::default();
            let expense = Expense::sample();
            add_transaction_to_db(&db, expense.transaction());

            // Test
            DeleteExpense::delete(&db, expense.id())?;

            // Assertions
            check_transaction_not_in_db(&db, expense.id());
            Ok(())
        }

        #[test]
        fn should_error_if_does_not_exist() {
            // Setup
            let db = InMemoryDatabase::default();
            let expense = Expense::sample();

            // Test
            let res = DeleteExpense::delete(&db, expense.id());

            // Assertions
            assert!(
                matches!(res, Err(err) if err.to_string().starts_with("Transaction does not exist"))
            );
        }
    }
}
