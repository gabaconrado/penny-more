use penny_more::{AddEarning, DeleteEarning, ReadEarning, UpdateEarning};

use crate::InMemoryDatabase;

impl AddEarning for InMemoryDatabase {}

impl ReadEarning for InMemoryDatabase {}

impl UpdateEarning for InMemoryDatabase {}

impl DeleteEarning for InMemoryDatabase {}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::in_memory::transaction::tests::{
        add_transaction_to_db, check_transaction_not_in_db, get_transaction_from_db,
    };
    use penny_more::{Earning, TransactionId};

    mod add {
        use super::*;

        #[test]
        fn should_add_earning() -> anyhow::Result<()> {
            // Setup
            let db = InMemoryDatabase::default();
            let earning = Earning::sample();
            check_transaction_not_in_db(&db, earning.id());

            // Test
            db.add(&earning)?;

            // Assertions
            let tr = get_transaction_from_db(&db, earning.id());
            assert_eq!(tr, *earning.transaction());
            Ok(())
        }

        #[test]
        fn should_error_if_exists() {
            // Setup
            let db = InMemoryDatabase::default();
            let earning = Earning::sample();
            add_transaction_to_db(&db, earning.transaction());

            // Test
            let res = db.add(&earning);

            // Assertions
            assert!(matches!(res, Err(err) if err.to_string() == "Transaction already exists"));
        }
    }

    mod read {

        use super::*;

        #[test]
        fn should_read_all() -> anyhow::Result<()> {
            // Setup
            let db = InMemoryDatabase::default();
            let earning = Earning::sample();
            add_transaction_to_db(&db, earning.transaction());

            // Test
            let earnings = db.read_all()?;

            // Assertions
            assert!(matches!(earnings.as_slice(), [e] if *e == earning));
            Ok(())
        }

        #[test]
        fn should_read() -> anyhow::Result<()> {
            // Setup
            let db = InMemoryDatabase::default();
            let earning = Earning::sample();
            add_transaction_to_db(&db, earning.transaction());

            // Test
            let saved = db.read(earning.id())?;

            // Assertions
            assert_eq!(earning, saved);
            Ok(())
        }

        #[test]
        fn read_should_fail_if_earning_does_not_exist() {
            // Setup
            let db = InMemoryDatabase::default();

            // Test
            let res = db.read(TransactionId::sample());
            println!("{res:?}");

            // Assertions
            assert!(
                matches!(res, Err(err) if err.to_string().contains("Requested transaction does not exist"))
            );
        }
    }

    mod update {
        use super::*;

        #[test]
        fn should_update() -> anyhow::Result<()> {
            // Setup
            let db = InMemoryDatabase::default();
            let earning = Earning::sample();
            add_transaction_to_db(&db, earning.transaction());
            let new_earning = Earning::sample_with_id(earning.id());

            // Test
            db.update(&new_earning)?;

            // Assertions
            let in_db = get_transaction_from_db(&db, new_earning.id());
            assert_eq!(in_db, *new_earning.transaction());
            Ok(())
        }

        #[test]
        fn should_error_if_does_not_exist() {
            // Setup
            let db = InMemoryDatabase::default();
            let earning = Earning::sample();

            // Test
            let res = db.update(&earning);

            // Assertions
            assert!(
                matches!(res, Err(err) if err.to_string().starts_with("Transaction does not exist"))
            );
        }
    }

    mod delete {
        use super::*;

        #[test]
        fn should_delete() -> anyhow::Result<()> {
            // Setup
            let db = InMemoryDatabase::default();
            let earning = Earning::sample();
            add_transaction_to_db(&db, earning.transaction());

            // Test
            DeleteEarning::delete(&db, earning.id())?;

            // Assertions
            check_transaction_not_in_db(&db, earning.id());
            Ok(())
        }

        #[test]
        fn should_error_if_does_not_exist() {
            // Setup
            let db = InMemoryDatabase::default();
            let earning = Earning::sample();

            // Test
            let res = DeleteEarning::delete(&db, earning.id());

            // Assertions
            assert!(
                matches!(res, Err(err) if err.to_string().starts_with("Transaction does not exist"))
            );
        }
    }
}
