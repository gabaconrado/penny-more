use penny_more::{Account, AccountId, AddAccount, DeleteAccount, ReadAccount, UpdateAccount};

use crate::{InMemoryDatabase, InMemoryDatabaseError};

impl AddAccount for InMemoryDatabase {
    /// Adds a new account into the database
    ///
    /// # Errors
    ///
    /// - Account with same ID exists;
    /// - Internal error;
    fn add(&self, account: &Account) -> anyhow::Result<()> {
        match self.get_account_by_id(account.id()) {
            Err(InMemoryDatabaseError::AccountDoesNotExist(_)) => {
                let _added = self.add_or_update_account(account)?;
            }
            Ok(_) => anyhow::bail!("Account already exists"),
            Err(err) => anyhow::bail!("Internal error: {err}"),
        };
        Ok(())
    }
}

impl ReadAccount for InMemoryDatabase {
    /// Reads all saved accounts from the database
    ///
    /// # Errors
    ///
    /// - Internal error;
    fn read_all(&self) -> anyhow::Result<Vec<Account>> {
        let accounts = self.get_all_accounts()?;
        Ok(accounts)
    }

    /// Reads a saved account by its ID
    ///
    /// # Errors
    ///
    /// - Account does not exist;
    /// - Internal;
    fn read(&self, id: AccountId) -> anyhow::Result<Account> {
        let account = self.get_account_by_id(id)?;
        Ok(account)
    }
}

impl UpdateAccount for InMemoryDatabase {
    /// Updates an account in the database
    ///
    /// # Errors
    ///
    /// - Account does not exist;
    /// - Internal error;
    fn update(&self, account: &Account) -> anyhow::Result<()> {
        match self.get_account_by_id(account.id()) {
            Ok(_) => {
                let _ = self.add_or_update_account(account)?;
            }
            Err(InMemoryDatabaseError::AccountDoesNotExist(id)) => {
                anyhow::bail!("Account does not exist: {id}")
            }
            Err(err) => anyhow::bail!("Internal error: {err}"),
        };

        Ok(())
    }
}

impl DeleteAccount for InMemoryDatabase {
    /// Deletes an account from the database
    ///
    /// # Errors
    ///
    /// - Account does not exist;
    /// - Internal error;
    fn delete(&self, account_id: AccountId) -> anyhow::Result<()> {
        match self.get_account_by_id(account_id) {
            Ok(_) => {
                let _ = self.delete_account(account_id)?;
            }
            Err(InMemoryDatabaseError::AccountDoesNotExist(id)) => {
                anyhow::bail!("Account does not exist: {id}")
            }
            Err(err) => anyhow::bail!("Internal error: {err}"),
        };

        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::in_memory::account::tests::{
        add_account_to_db, check_account_not_in_db, get_account_from_db,
    };

    mod add {
        use super::*;

        #[test]
        fn should_add_account() -> anyhow::Result<()> {
            // Setup
            let db = InMemoryDatabase::default();
            let account = Account::sample();
            check_account_not_in_db(&db, account.id());

            // Test
            db.add(&account)?;

            // Assertions
            let acc = get_account_from_db(&db, account.id());
            assert_eq!(acc, account);
            Ok(())
        }

        #[test]
        fn should_error_if_exists() {
            // Setup
            let db = InMemoryDatabase::default();
            let account = Account::sample();
            add_account_to_db(&db, &account);

            // Test
            let res = db.add(&account);

            // Assertions
            assert!(matches!(res, Err(err) if err.to_string() == "Account already exists"));
        }
    }

    mod read {
        use super::*;

        #[test]
        fn should_read_all() -> anyhow::Result<()> {
            // Setup
            let db = InMemoryDatabase::default();
            let account = Account::sample();
            let _updated = db.add_or_update_account(&account)?;

            // Test
            let accounts = db.read_all()?;

            // Assertions
            assert!(matches!(accounts.as_slice(), [acc] if *acc == account));
            Ok(())
        }

        #[test]
        fn should_read() -> anyhow::Result<()> {
            // Setup
            let db = InMemoryDatabase::default();
            let account = Account::sample();
            let _updated = db.add_or_update_account(&account)?;

            // Test
            let saved = db.read(account.id())?;

            // Assertions
            assert_eq!(account, saved);
            Ok(())
        }

        #[test]
        fn read_should_fail_if_account_does_not_exist() {
            // Setup
            let db = InMemoryDatabase::default();

            // Test
            let res = db.read(AccountId::sample());
            println!("{res:?}");

            // Assertions
            assert!(
                matches!(res, Err(err) if err.to_string().contains("Requested account does not exist"))
            );
        }
    }

    mod update {
        use penny_more::{test_helpers, MonetaryValue};

        use super::*;

        #[test]
        fn should_update() -> anyhow::Result<()> {
            // Setup
            let db = InMemoryDatabase::default();
            let account = Account::sample();
            let _updated = db.add_or_update_account(&account)?;
            let new_account = Account::new(
                account.id(),
                test_helpers::random_string(16),
                MonetaryValue::sample(),
            );

            // Test
            db.update(&new_account)?;

            // Assertions
            let in_db = get_account_from_db(&db, new_account.id());
            assert_eq!(in_db, new_account);
            Ok(())
        }

        #[test]
        fn should_error_if_does_not_exist() {
            // Setup
            let db = InMemoryDatabase::default();
            let account = Account::sample();

            // Test
            let res = db.update(&account);

            // Assertions
            assert!(
                matches!(res, Err(err) if err.to_string().starts_with("Account does not exist"))
            );
        }
    }

    mod delete {
        use super::*;

        #[test]
        fn should_delete() -> anyhow::Result<()> {
            // Setup
            let db = InMemoryDatabase::default();
            let account = Account::sample();
            let _updated = db.add_or_update_account(&account)?;

            // Test
            DeleteAccount::delete(&db, account.id())?;

            // Assertions
            check_account_not_in_db(&db, account.id());
            Ok(())
        }

        #[test]
        fn should_error_if_does_not_exist() {
            // Setup
            let db = InMemoryDatabase::default();
            let account = Account::sample();

            // Test
            let res = DeleteAccount::delete(&db, account.id());

            // Assertions
            assert!(
                matches!(res, Err(err) if err.to_string().starts_with("Account does not exist"))
            );
        }
    }
}
