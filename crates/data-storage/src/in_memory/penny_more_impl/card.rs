use penny_more::{AddCard, Card, CardId, DeleteCard, ReadCard, UpdateCard};

use crate::{InMemoryDatabase, InMemoryDatabaseError};

impl AddCard for InMemoryDatabase {
    /// Adds a new card into the database
    ///
    /// # Errors
    ///
    /// - Card with same ID exists;
    /// - Internal error;
    fn add(&self, card: &Card) -> anyhow::Result<()> {
        match self.get_card_by_id(card.id()) {
            Err(InMemoryDatabaseError::CardDoesNotExist(_)) => {
                let _added = self.add_or_update_card(card)?;
            }
            Ok(_) => anyhow::bail!("Card already exists"),
            Err(err) => anyhow::bail!("Internal error: {err}"),
        };
        Ok(())
    }
}

impl ReadCard for InMemoryDatabase {
    /// Reads all saved cards from the database
    ///
    /// # Errors
    ///
    /// - Internal error;
    fn read_all(&self) -> anyhow::Result<Vec<Card>> {
        let cards = self.get_all_cards()?;
        Ok(cards)
    }

    /// Reads a saved card by its ID
    ///
    /// # Errors
    ///
    /// - Card does not exist;
    /// - Internal;
    fn read(&self, id: CardId) -> anyhow::Result<Card> {
        let card = self.get_card_by_id(id)?;
        Ok(card)
    }
}

impl UpdateCard for InMemoryDatabase {
    /// Updates an card in the database
    ///
    /// # Errors
    ///
    /// - Card does not exist;
    /// - Internal error;
    fn update(&self, card: &Card) -> anyhow::Result<()> {
        match self.get_card_by_id(card.id()) {
            Ok(_) => {
                let _ = self.add_or_update_card(card)?;
            }
            Err(InMemoryDatabaseError::CardDoesNotExist(id)) => {
                anyhow::bail!("Card does not exist: {id}")
            }
            Err(err) => anyhow::bail!("Internal error: {err}"),
        };

        Ok(())
    }
}

impl DeleteCard for InMemoryDatabase {
    /// Deletes an card from the database
    ///
    /// # Errors
    ///
    /// - Card does not exist;
    /// - Internal error;
    fn delete(&self, card_id: CardId) -> anyhow::Result<()> {
        match self.get_card_by_id(card_id) {
            Ok(_) => {
                let _ = self.delete_card(card_id)?;
            }
            Err(InMemoryDatabaseError::CardDoesNotExist(id)) => {
                anyhow::bail!("Card does not exist: {id}")
            }
            Err(err) => anyhow::bail!("Internal error: {err}"),
        };

        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::in_memory::card::tests::{add_card_to_db, check_card_not_in_db, get_card_from_db};

    mod add {
        use super::*;

        #[test]
        fn should_add_card() -> anyhow::Result<()> {
            // Setup
            let db = InMemoryDatabase::default();
            let card = Card::sample();
            check_card_not_in_db(&db, card.id());

            // Test
            db.add(&card)?;

            // Assertions
            let acc = get_card_from_db(&db, card.id());
            assert_eq!(acc, card);
            Ok(())
        }

        #[test]
        fn should_error_if_exists() {
            // Setup
            let db = InMemoryDatabase::default();
            let card = Card::sample();
            add_card_to_db(&db, &card);

            // Test
            let res = db.add(&card);

            // Assertions
            assert!(matches!(res, Err(err) if err.to_string() == "Card already exists"));
        }
    }

    mod read {
        use super::*;

        #[test]
        fn should_read_all() -> anyhow::Result<()> {
            // Setup
            let db = InMemoryDatabase::default();
            let card = Card::sample();
            let _updated = db.add_or_update_card(&card)?;

            // Test
            let cards = db.read_all()?;

            // Assertions
            assert!(matches!(cards.as_slice(), [acc] if *acc == card));
            Ok(())
        }

        #[test]
        fn should_read() -> anyhow::Result<()> {
            // Setup
            let db = InMemoryDatabase::default();
            let card = Card::sample();
            let _updated = db.add_or_update_card(&card)?;

            // Test
            let saved = db.read(card.id())?;

            // Assertions
            assert_eq!(card, saved);
            Ok(())
        }

        #[test]
        fn read_should_fail_if_card_does_not_exist() {
            // Setup
            let db = InMemoryDatabase::default();

            // Test
            let res = db.read(CardId::sample());
            println!("{res:?}");

            // Assertions
            assert!(
                matches!(res, Err(err) if err.to_string().contains("Requested card does not exist"))
            );
        }
    }

    mod update {
        use penny_more::{test_helpers, AccountId, MonetaryValue};

        use super::*;

        #[test]
        fn should_update() -> anyhow::Result<()> {
            // Setup
            let db = InMemoryDatabase::default();
            let card = Card::sample();
            let _updated = db.add_or_update_card(&card)?;
            let new_card = Card::new(
                card.id(),
                test_helpers::random_string(16),
                MonetaryValue::sample(),
                Some(AccountId::sample()),
            );

            // Test
            db.update(&new_card)?;

            // Assertions
            let in_db = get_card_from_db(&db, new_card.id());
            assert_eq!(in_db, new_card);
            Ok(())
        }

        #[test]
        fn should_error_if_does_not_exist() {
            // Setup
            let db = InMemoryDatabase::default();
            let card = Card::sample();

            // Test
            let res = db.update(&card);

            // Assertions
            assert!(matches!(res, Err(err) if err.to_string().starts_with("Card does not exist")));
        }
    }

    mod delete {
        use super::*;

        #[test]
        fn should_delete() -> anyhow::Result<()> {
            // Setup
            let db = InMemoryDatabase::default();
            let card = Card::sample();
            let _updated = db.add_or_update_card(&card)?;

            // Test
            DeleteCard::delete(&db, card.id())?;

            // Assertions
            check_card_not_in_db(&db, card.id());
            Ok(())
        }

        #[test]
        fn should_error_if_does_not_exist() {
            // Setup
            let db = InMemoryDatabase::default();
            let card = Card::sample();

            // Test
            let res = DeleteCard::delete(&db, card.id());

            // Assertions
            assert!(matches!(res, Err(err) if err.to_string().starts_with("Card does not exist")));
        }
    }
}
