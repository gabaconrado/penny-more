use penny_more::{
    AddTransaction, DeleteTransaction, ReadTransaction, Transaction, TransactionId,
    UpdateTransaction,
};

use crate::{InMemoryDatabase, InMemoryDatabaseError};

impl AddTransaction for InMemoryDatabase {
    /// Adds a new transaction into the database
    ///
    /// # Errors
    ///
    /// - Transaction with same ID exists;
    /// - Internal error;
    fn add(&self, transaction: &Transaction) -> anyhow::Result<()> {
        match self.get_transaction_by_id(transaction.id()) {
            Err(InMemoryDatabaseError::TransactionDoesNotExist(_)) => {
                let _added = self.add_or_update_transaction(transaction)?;
            }
            Ok(_) => anyhow::bail!("Transaction already exists"),
            Err(err) => anyhow::bail!("Internal error: {err}"),
        };
        Ok(())
    }
}

impl ReadTransaction for InMemoryDatabase {
    /// Reads all saved transactions from the database
    ///
    /// # Errors
    ///
    /// - Internal error;
    fn read_all(&self) -> anyhow::Result<Vec<Transaction>> {
        let transactions = self.get_all_transactions()?;
        Ok(transactions)
    }

    /// Reads a saved transaction by its ID
    ///
    /// # Errors
    ///
    /// - Transaction does not exist;
    /// - Internal;
    fn read(&self, id: TransactionId) -> anyhow::Result<Transaction> {
        let transaction = self.get_transaction_by_id(id)?;
        Ok(transaction)
    }
}

impl UpdateTransaction for InMemoryDatabase {
    /// Updates a transaction in the database
    ///
    /// # Errors
    ///
    /// - Transaction does not exist;
    /// - Internal error;
    fn update(&self, transaction: &Transaction) -> anyhow::Result<()> {
        match self.get_transaction_by_id(transaction.id()) {
            Ok(_) => {
                let _ = self.add_or_update_transaction(transaction)?;
            }
            Err(InMemoryDatabaseError::TransactionDoesNotExist(id)) => {
                anyhow::bail!("Transaction does not exist: {id}")
            }
            Err(err) => anyhow::bail!("Internal error: {err}"),
        };

        Ok(())
    }
}

impl DeleteTransaction for InMemoryDatabase {
    /// Deletes a transaction from the database
    ///
    /// # Errors
    ///
    /// - Transaction does not exist;
    /// - Internal error;
    fn delete(&self, transaction_id: TransactionId) -> anyhow::Result<()> {
        match self.get_transaction_by_id(transaction_id) {
            Ok(_) => {
                let _ = self.delete_transaction(transaction_id)?;
            }
            Err(InMemoryDatabaseError::TransactionDoesNotExist(id)) => {
                anyhow::bail!("Transaction does not exist: {id}")
            }
            Err(err) => anyhow::bail!("Internal error: {err}"),
        };

        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::in_memory::transaction::tests::{
        add_transaction_to_db, check_transaction_not_in_db, get_transaction_from_db,
    };

    mod add {
        use super::*;

        #[test]
        fn should_add_transaction() -> anyhow::Result<()> {
            // Setup
            let db = InMemoryDatabase::default();
            let transaction = Transaction::sample();
            check_transaction_not_in_db(&db, transaction.id());

            // Test
            db.add(&transaction)?;

            // Assertions
            let acc = get_transaction_from_db(&db, transaction.id());
            assert_eq!(acc, transaction);
            Ok(())
        }

        #[test]
        fn should_error_if_exists() {
            // Setup
            let db = InMemoryDatabase::default();
            let transaction = Transaction::sample();
            add_transaction_to_db(&db, &transaction);

            // Test
            let res = db.add(&transaction);

            // Assertions
            assert!(matches!(res, Err(err) if err.to_string() == "Transaction already exists"));
        }
    }

    mod read {
        use super::*;

        #[test]
        fn should_read_all() -> anyhow::Result<()> {
            // Setup
            let db = InMemoryDatabase::default();
            let transaction = Transaction::sample();
            let _updated = db.add_or_update_transaction(&transaction)?;

            // Test
            let transactions = db.read_all()?;

            // Assertions
            assert!(matches!(transactions.as_slice(), [acc] if *acc == transaction));
            Ok(())
        }

        #[test]
        fn should_read() -> anyhow::Result<()> {
            // Setup
            let db = InMemoryDatabase::default();
            let transaction = Transaction::sample();
            let _updated = db.add_or_update_transaction(&transaction)?;

            // Test
            let saved = db.read(transaction.id())?;

            // Assertions
            assert_eq!(transaction, saved);
            Ok(())
        }

        #[test]
        fn read_should_fail_if_transaction_does_not_exist() {
            // Setup
            let db = InMemoryDatabase::default();

            // Test
            let res = db.read(TransactionId::sample());
            println!("{res:?}");

            // Assertions
            assert!(
                matches!(res, Err(err) if err.to_string().contains("Requested transaction does not exist"))
            );
        }
    }

    mod update {
        use super::*;

        #[test]
        fn should_update() -> anyhow::Result<()> {
            // Setup
            let db = InMemoryDatabase::default();
            let transaction = Transaction::sample();
            let _updated = db.add_or_update_transaction(&transaction)?;
            let new_transaction = Transaction::sample_with_id(transaction.id());
            // Test
            db.update(&new_transaction)?;

            // Assertions
            let in_db = get_transaction_from_db(&db, new_transaction.id());
            assert_eq!(in_db, new_transaction);
            Ok(())
        }

        #[test]
        fn should_error_if_does_not_exist() {
            // Setup
            let db = InMemoryDatabase::default();
            let transaction = Transaction::sample();

            // Test
            let res = db.update(&transaction);

            // Assertions
            assert!(
                matches!(res, Err(err) if err.to_string().starts_with("Transaction does not exist"))
            );
        }
    }

    mod delete {
        use super::*;

        #[test]
        fn should_delete() -> anyhow::Result<()> {
            // Setup
            let db = InMemoryDatabase::default();
            let transaction = Transaction::sample();
            let _updated = db.add_or_update_transaction(&transaction)?;

            // Test
            DeleteTransaction::delete(&db, transaction.id())?;

            // Assertions
            check_transaction_not_in_db(&db, transaction.id());
            Ok(())
        }

        #[test]
        fn should_error_if_does_not_exist() {
            // Setup
            let db = InMemoryDatabase::default();
            let transaction = Transaction::sample();

            // Test
            let res = DeleteTransaction::delete(&db, transaction.id());

            // Assertions
            assert!(
                matches!(res, Err(err) if err.to_string().starts_with("Transaction does not exist"))
            );
        }
    }
}
