use std::{collections::HashMap, sync::Mutex};

use penny_more::{Account, AccountId, Card, CardId, Transaction, TransactionId};

/// An in-memory database implementation
#[derive(Debug, Default)]
pub struct InMemoryDatabase {
    /// Account saved objects
    pub(super) accounts: Mutex<HashMap<AccountId, Account>>,
    /// Card saved objects
    pub(super) cards: Mutex<HashMap<CardId, Card>>,
    /// Transaction saved objects
    pub(super) transactions: Mutex<HashMap<TransactionId, Transaction>>,
}

impl InMemoryDatabase {
    /// Creates a new empty [InMemoryDatabase]
    pub fn new() -> Self {
        Self::default()
    }
}
