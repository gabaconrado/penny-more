use penny_more::{Account, AccountId};

use super::error::Error;
use crate::InMemoryDatabase;

impl InMemoryDatabase {
    /// Inserts or updates an existing account
    ///
    /// Returns true if account was created, false otherwise
    ///
    /// # Errors
    ///
    /// - Internal lock poisoned;
    pub fn add_or_update_account(&self, account: &Account) -> Result<bool, Error> {
        let updated = self.accounts.lock()?.insert(account.id(), account.clone());
        Ok(updated.is_none())
    }

    /// Reads all accounts from the internal map
    ///
    /// # Errors
    ///
    /// - Internal lock poisoned;
    pub fn get_all_accounts(&self) -> Result<Vec<Account>, Error> {
        let accounts = self
            .accounts
            .lock()?
            .values()
            .cloned()
            .collect::<Vec<Account>>();
        Ok(accounts)
    }

    /// Returns an account by its identifier from the internal map
    ///
    /// # Errors
    ///
    /// - Account does not exist;
    /// - Internal lock poisoned;
    pub fn get_account_by_id(&self, id: AccountId) -> Result<Account, Error> {
        let account = self
            .accounts
            .lock()?
            .get(&id)
            .ok_or(Error::AccountDoesNotExist(id))?
            .clone();
        Ok(account)
    }

    /// Delete an account from the internal map
    ///
    /// Returns true if any account was deleted, false otherwise
    ///
    /// # Errors
    ///
    /// - Internal lock poisoned;
    pub fn delete_account(&self, id: AccountId) -> Result<bool, Error> {
        let removed = self.accounts.lock()?.remove(&id);
        Ok(removed.is_some())
    }
}

#[cfg(test)]
pub(crate) mod tests {
    use super::*;

    mod get {
        use super::*;

        #[test]
        fn should_get_all() -> Result<(), Error> {
            // Setup
            let db = InMemoryDatabase::default();
            let accounts = [Account::sample(), Account::sample()];
            accounts.iter().for_each(|a| add_account_to_db(&db, a));

            // Test
            let all = db.get_all_accounts()?;

            // Assertions
            assert!(all.iter().all(|a| accounts.contains(a)));
            Ok(())
        }

        #[test]
        fn should_get_by_id() -> Result<(), Error> {
            // Setup
            let db = InMemoryDatabase::default();
            let account = Account::sample();
            add_account_to_db(&db, &account);

            // Test
            let acc = db.get_account_by_id(account.id())?;

            // Assertions
            assert_eq!(acc, account);
            Ok(())
        }

        #[test]
        fn should_error_if_does_not_exist() {
            // Setup
            let db = InMemoryDatabase::default();
            let id = AccountId::sample();

            // Test
            let res = db.get_account_by_id(id);

            // Assertions
            assert!(matches!(res, Err(Error::AccountDoesNotExist(i)) if i == id));
        }
    }

    mod add_or_update {
        use penny_more::{test_helpers, MonetaryValue};

        use super::*;

        #[test]
        fn should_add() -> Result<(), Error> {
            // Setup
            let db = InMemoryDatabase::default();
            let account = Account::sample();

            // Test
            let added = db.add_or_update_account(&account)?;

            // Assertions
            let acc = get_account_from_db(&db, account.id());
            assert!(added);
            assert_eq!(acc, account);
            Ok(())
        }

        #[test]
        fn should_update() -> Result<(), Error> {
            // Setup
            let db = InMemoryDatabase::default();
            let account = Account::sample();
            add_account_to_db(&db, &account);
            let (new_name, new_balance) =
                (test_helpers::random_string(16), MonetaryValue::sample());
            let updated = Account::new(account.id(), new_name, new_balance);

            // Test
            let added = db.add_or_update_account(&updated)?;

            // Assertions
            let acc = get_account_from_db(&db, account.id());
            assert!(!added);
            assert_eq!(acc, updated);
            Ok(())
        }
    }

    mod delete {
        use super::*;

        #[test]
        fn should_delete() -> Result<(), Error> {
            // Setup
            let db = InMemoryDatabase::default();
            let account = Account::sample();
            add_account_to_db(&db, &account);

            // Test
            let deleted = db.delete_account(account.id())?;
            let not_deleted = db.delete_account(AccountId::sample())?;

            // Assertions
            assert!(deleted);
            assert!(!not_deleted);
            check_account_not_in_db(&db, account.id());
            Ok(())
        }
    }

    /// Adds an account to the db for testing
    pub(crate) fn add_account_to_db(db: &InMemoryDatabase, acc: &Account) {
        let added = db
            .accounts
            .lock()
            .expect("Could not lock database mutex")
            .insert(acc.id(), acc.clone());
        assert!(added.is_none());
    }

    /// Gets an account from the db for testing
    pub(crate) fn get_account_from_db(db: &InMemoryDatabase, id: AccountId) -> Account {
        let account = db
            .accounts
            .lock()
            .expect("Could not lock database mutex")
            .get(&id)
            .expect("Account does not exist in the database")
            .clone();
        account
    }

    /// Asserts that the passed account is not in the database
    pub(crate) fn check_account_not_in_db(db: &InMemoryDatabase, id: AccountId) {
        let not_in_db = db
            .accounts
            .lock()
            .expect("Could not lock database mutex")
            .get(&id)
            .is_none();
        assert!(not_in_db);
    }
}
